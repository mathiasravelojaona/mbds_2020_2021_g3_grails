package Forms.Classes

import com.mbds.grails.Role
import com.mbds.grails.User
import grails.validation.Validateable

class CreateUserForm implements Validateable{

    String username
    String password
    String role


    static constraints = {
        username nullable: false, blank: false
        password nullable: false, blank: false,password: true
        role nullable: false, blank: false, validator: {value ->
            try{
                def role = Role.findById(Long.valueOf(value))
                if(!role){
                    throw new Exception()
                }
                return true
            }
            catch (Exception e){
                errors.rejectValue('role','blank')
                return false
            }
        }
    }
}
