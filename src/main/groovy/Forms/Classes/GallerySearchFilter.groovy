package Forms.Classes

import java.text.SimpleDateFormat

class GallerySearchFilter {
    private String keyword
    private Long[] authors
    private Date d1
    private Date d2
    private String order
    private boolean galleryDate
    private Integer page

    public static final String ASC = "ASC"
    public static final String DESC = "DESC"

    void setPage(String page){
         try{
             this.page = Integer.parseInt(page)
         }
         catch (Exception e){
             this.page = 0
         }
    }
    int getPage(){
        return this.page
    }

    String getKeyword() {
        return keyword
    }

    void setKeyword(String keyword) {
        this.keyword = keyword ? keyword : ""
    }

    Long[] getAuthors() {
        return authors
    }

    void setAuthors(String authors) {
        try{
            String[] temp_authors = authors.split(',')
            if(temp_authors.length == 0){
                this.authors = null
            }
            else{
                Long[]listeIds = new Long[temp_authors.length]
                temp_authors.eachWithIndex {it,i ->
                    listeIds[i] = Long.parseLong(it)
                }
                this.authors = listeIds
            }
        }
        catch (Exception e){
            this.authors = null
        }
    }

    Date getD1() {
        return d1
    }

    void setD1(String d1) {
        try{
            this.d1 = new SimpleDateFormat('dd/MM/yyyy').parse(d1)
            Calendar c1 = Calendar.getInstance()
            c1.setTimeInMillis(this.d1.getTime())
            c1.set(Calendar.HOUR_OF_DAY, 0)
            c1.set(Calendar.MINUTE, 0)
            c1.set(Calendar.SECOND, 0)
            c1.set(Calendar.MILLISECOND, 0)
            this.d1.setTime(c1.getTimeInMillis())
        }
        catch (Exception e){
            this.d1 = null
        }
    }

    Date getD2() {
        return d2
    }

    void setD2(String d2) {
        try{
            this.d2 = new SimpleDateFormat('dd/MM/yyyy').parse(d2)
            Calendar c1 = Calendar.getInstance()
            c1.setTimeInMillis(this.d2.getTime())
            c1.set(Calendar.HOUR_OF_DAY, 23)
            c1.set(Calendar.MINUTE, 59)
            c1.set(Calendar.SECOND, 59)
            c1.set(Calendar.MILLISECOND, 0)
            this.d2.setTime(c1.getTimeInMillis())
        }
        catch (Exception e){
            this.d2 = null
        }
    }

    String getOrder() {
        return order
    }

    void setOrder(String order) {
        try{
            this.order = order.toUpperCase().equals(ASC) ? ASC: DESC
        }
        catch (Exception e){
            this.order = ASC
        }
    }

    boolean getGalleryDate() {
        return galleryDate
    }

    void setGalleryDate(String galleryDate) {
        this.galleryDate = galleryDate == "true"
    }

    GallerySearchFilter(String keyword,
                        String authors,
                        String d1,
                        String d2,
                        String order,
                        String galleryDate,
                        String page) {

        this.setKeyword(keyword)
        this.setAuthors(authors)
        this.setD1(d1)
        this.setD2(d2)
        this.setOrder(order)
        this.setGalleryDate(galleryDate)
        this.setPage(page)
    }
}
