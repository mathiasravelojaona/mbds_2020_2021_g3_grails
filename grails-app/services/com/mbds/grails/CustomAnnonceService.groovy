package com.mbds.grails

import Forms.Classes.InsertAnnonceForm
import Forms.Classes.UpdateAnnonceForm
import grails.gorm.transactions.Transactional
import grails.orm.PagedResultList
import grails.validation.ValidationException
import grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.web.multipart.MultipartFile


@Transactional
class CustomAnnonceService {

    def grailsApplication
    CDNService CDNService
    ObjectMapperService ObjectMapperService

    Annonce SaveWithImage(InsertAnnonceForm insertAnnonceForm, MultipartFile picture){
            if(picture.empty){
                throw new Exception("le fichier que vous tentez d'utiliser est vide")
            }
            if(!insertAnnonceForm.validate()){
                throw new ValidationException('Une erreur est survenue',insertAnnonceForm.errors)
            }
            Annonce annonce = ObjectMapperService.map(insertAnnonceForm)
            Illustration newillustration = new Illustration()
            newillustration.filename = CDNService.doUpload(picture).get('url').toString()
            annonce.addToIllustrations(newillustration)
            annonce.save(true)
    }

    Annonce updateAnnonce(UpdateAnnonceForm updateFormModel){
        Annonce annonce = ObjectMapperService.map(updateFormModel)
        annonce.save()
        return annonce
    }


    def list(GrailsParameterMap params, String searchkey){
        println("*************** "+ searchkey)
        PagedResultList<Annonce> annonceList = Annonce.createCriteria().list(params) {
            if (searchkey != null) {
                or {
                    and {
                        ilike("title", "%" + searchkey + "%")
                    }
                    and {
                        println("*************** description")
                        ilike("description", "%" + searchkey + "%")
                    }
                }
            }
        }
        return [annonceList: annonceList, count: annonceList.totalCount]
    }

    def count(){
        return [Annonce.count()]
    }

    def findAll(){
        Annonce.findAll()
    }
}
