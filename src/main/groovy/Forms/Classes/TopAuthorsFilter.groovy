package Forms.Classes

import java.lang.reflect.Array

class TopAuthorsFilter {
    private Integer top = 5
    private Integer month

    Integer getTop() {
        return top
    }

    void setTop(String top) {
            if(["5","10","15"].contains(top)){
                this.top = Integer.valueOf(top)
            }
    }

    Integer getMonth() {
        return month
    }

    void setMonth(String month) {
        try{
            this.month = Integer.valueOf(month)
            if(this.month<1 || this.month>12){
                throw new Exception()
            }
        }
        catch (Exception e){
            this.month = null
        }
    }
    TopAuthorsFilter(String top, String month){
        this.setMonth(month)
        this.setTop(top)
    }
}
