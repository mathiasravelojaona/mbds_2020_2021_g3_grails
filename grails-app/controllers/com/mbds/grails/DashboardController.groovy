package com.mbds.grails

import Forms.Classes.SearchDaysFilter
import Forms.Classes.SearchDowFilter
import Forms.Classes.SearchMonthFilter
import Forms.Classes.TopAuthorsFilter
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN', 'ROLE_MODO'])
class DashboardController {

    DashboardService dashboardService

    def index() {
        [current_date: new Date()]
    }

    def statistiques(){
        render dashboardService.statistiques() as JSON
    }
    def articlesParMois(){
        SearchMonthFilter searchMonthFilter = new SearchMonthFilter(params.year)
        def map = [filters: searchMonthFilter,stats: this.dashboardService.nombreArticlesParMois(searchMonthFilter)]
        render map as JSON
    }

    //Articles par jour de la semaine , dow = day of week
    def articlesParDow(){
        SearchDowFilter searchDowFilter = new SearchDowFilter(params.year,params.month)
        def map = [filters: searchDowFilter,stats: this.dashboardService.nombreArticlesParJourDeLaSemaine(searchDowFilter)]
        render map as JSON
    }


    def articlesParJour(){
        SearchDaysFilter searchDaysFilter = new SearchDaysFilter(params.d1,params.d2)
        def map = [filters: searchDaysFilter,stats: this.dashboardService.nombreArticlesParJour(searchDaysFilter)]
        render map as JSON
    }

    def topAuthors(){
        TopAuthorsFilter topAuthorsFilter = new TopAuthorsFilter(params.top,params.month)
        def map = [filters: topAuthorsFilter,stats: this.dashboardService.topUtilisateurs(topAuthorsFilter)]
        render map as JSON
    }
}
