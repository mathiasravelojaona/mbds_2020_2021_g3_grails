package Forms.Classes

class SearchDowFilter {

    private Integer year
    private Integer month

    void setYear(String year){
        try{
            this.year = Integer.valueOf(year)
        }
        catch (Exception e){
            this.year = null
        }
    }
    void setMonth(String month){
        try{
            this.month = Integer.valueOf(month)
            if(this.month<1 || this.month>12){
                throw new Exception()
            }
        }
        catch (Exception e){
            this.month = null
        }
    }
    Integer getMonth(){
        return this.month
    }
    Integer getYear(){
        return this.year
    }
    SearchDowFilter(String year, String month){
        this.setYear(year)
        this.setMonth(month)
    }
}
