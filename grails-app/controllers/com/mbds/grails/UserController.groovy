package com.mbds.grails

import Forms.Classes.CreateUserForm
import grails.converters.JSON
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured
import grails.plugin.springsecurity.userdetails.GrailsUser
import grails.validation.ValidationException

import javax.servlet.http.HttpServletResponse
import java.text.SimpleDateFormat
import org.springframework.web.multipart.MultipartFile
import static org.springframework.http.HttpStatus.*

@Secured(['ROLE_ADMIN', 'ROLE_MODO'])
class UserController {

    UserService userService

    UserRoleService userRoleService
    CustomUserService customUserService
    def springSecurityService

    def exportService

    static allowedMethods = [save: "POST", update: "PUT",configurerPassword: "POST",configurerUsername: "POST"]


    /*@org.springframework.security.access.annotation.Secured('permitAll')*/
    def logout(){
        redirect(uri: SpringSecurityUtils.securityConfig.logout.filterProcessesUrl + "?spring-security-redirect=/login1Page")
    }
    def index() {
        String searchkey = params.searchkey ? params.searchkey : ''
        String status = params.status ? params.status : null
        def users = customUserService.list(params, searchkey, status)
        respond users.list, model:[userCount: users.count,text: searchkey,status: status]
    }

    def export(){
        params.exportFormat = "excel"
        params.extension = "xls"
            def users = User.findAll()
            def result = []
            users.each {a ->
                def map = [:]
                map.put("username", a.username)
                map.put("role", a.authorities.find().authority)
                map.put("accountExpired", a.accountExpired)
                map.put("accountLocked", a.accountLocked)
                map.put("passwordExpired", a.passwordExpired)
                result.add(map)
            }
            response.contentType = grailsApplication.config.grails.mime.types[params.exportFormat]
            response.setHeader("Content-disposition", "attachment; filename=users-"+new SimpleDateFormat("ddMMyyy").format(new Date())+".xls")
            List fields = [
                    "username",
                    "role",
                    "accountExpired",
                    "accountLocked",
                    "passwordExpired",
            ]
            Map labels = ["username": "Nom d'utilisateur", "role": "Rôle", "accountExpired": "Compte expiré", "accountLocked": "Compte bloqué", "passwordExpired": "Mot de passe expiré"]
            Map formatters = [:]
            Map parameters = [:]
            exportService.export(params.exportFormat, response.outputStream, result, fields, labels, formatters, parameters)
    }

    def show(Long id) {
        respond userService.get(id)
    }

    @Secured(['ROLE_ADMIN'])
    def create() {
        [user: new CreateUserForm(), roleList: userRoleService.listRoles()]
    }

    @Secured(['ROLE_ADMIN'])
    def save(){
        def userform = new CreateUserForm(username: params.username,password: params.password,role: params.role)
        try {
            userRoleService.createUser(userform)
            flash.message = "l'utilisateur a été créé"
            redirect action: 'index', controllerName: 'user'
        }
        catch (ValidationException validationException){
            respond view: 'create',userform.errors, model: [user: userform, roleList: userRoleService.listRoles()]
            return
        }
        catch(Exception e){
            flash.message = e.message
            redirect action: 'create', controllerName: 'user'
        }
    }


    def edit(Long id) {
        def user = userService.get(id)
        user.password = ''
        def role = userRoleService.getUserRole(user)
        respond user, model: [roleList: userRoleService.listRoles(),userRole: role]
    }

    def update(User user) {
        if (user == null) {
            notFound()
            return
        }
        def currentUser = springSecurityService.getPrincipal() as GrailsUser
        try {
            user.enabled = params.enabled == "true"
            user.accountLocked = params.accountLocked == "true"
            user.accountExpired = params.accountExpired == "true"
            user.passwordExpired= params.passwordExpired == "true"
            def role = Role.findById(Long.valueOf(params.role))
            user = customUserService.updateUser(user,user.id == currentUser.id,role)
            flash.message = "L'utilisateur a été mis à jour avec succès"
            redirect action: 'edit', controllerName: 'user', id: user.id
        } catch (ValidationException e) {
            respond user.errors, view:'edit',model: ['user': user]
            return
        } catch(Exception e){
            flash.message = e.message
            redirect action: 'edit', controllerName: 'user',id: user.id
        }
    }

    @Secured(['ROLE_ADMIN'])
    def delete(Long id) {
        def user = userService.get(id)
        if (!user) {
            notFound()
            return
        }
        def currentUser = springSecurityService.getPrincipal() as GrailsUser
        try{
            if(user.id == currentUser.id){
                throw new Exception('Vous ne pouvez pas vous supprimer')
            }
            userRoleService.deleteUser(user)
            flash.message =  "L'utilisateur vient d'être supprimé"
            redirect action: 'index', controllerName: 'user'
        }
        catch (Exception e){
            flash.message =  e.message
            redirect action: 'index', controllerName: 'user'
        }
    }


    @Secured(['ROLE_ADMIN'])
    def upload(){
        try{
            userRoleService.uploadCSV(request)
            flash.message = "l'import s'est bien passé"
        }
        catch (Exception e){
            flash.message = "Une erreur est survenue"
        }
        finally {
            redirect(action: 'index')
        }
    }

    protected void notFound() {render status: NOT_FOUND }

    def updateStatusUser() {
        ResultJson resultJson = new ResultJson()
        resultJson.action = null
        resultJson.message = "La mise à jour du status est un succès !"
        resultJson.status = true
        try{
            long idUser = Long.valueOf(params.iduser)
            boolean status = params.boolean('status')
            def user = userService.get(idUser)
            if(!user){
                response.status = HttpServletResponse.SC_NOT_FOUND
                throw new Exception('Utilisateur non existant')
            }
            def currentUser = springSecurityService.getPrincipal() as GrailsUser
            if(user.id == currentUser.id){
                throw new Exception('Vous ne pouvez pas vous bloquer/débloquer')
            }
            user.enabled = status
            userService.save(user)
        }
        catch (Exception e){
            resultJson.message = e.message
            resultJson.status = false
            response.status = HttpServletResponse.SC_BAD_REQUEST
        }
        finally {
            render resultJson as JSON
        }
    }
    def configuration(){
        def user =  springSecurityService.currentUser
        respond user
    }
    def configurerUsername(){
        def currentUser = springSecurityService.currentUser
        def originalName = currentUser.username
        try{
            currentUser.username = params.username
            currentUser = customUserService.configurer(currentUser,false)
            flash.message = "Configuration achevée"
            redirect action: 'configuration'
        }
        catch (ValidationException e){
            currentUser.username = originalName
            respond currentUser.errors, view:'configuration',model: ['user': currentUser]
            return
        }
        catch(Exception e){
            flash.message = e.message
            redirect action: 'configuration', controllerName: 'user'
        }
    }
    def configurerPassword(){
        def currentUser = springSecurityService.currentUser
        try{
            currentUser.password = params.password
            currentUser = customUserService.configurer(currentUser,true)
            flash.message = "Configuration achevée"
            redirect action: 'configuration'
        }
        catch (ValidationException e){
            respond currentUser.errors, view:'configuration',model: ['user': currentUser]
            return
        }
        catch(Exception e){
            flash.message = e.message
            redirect action: 'configuration', controllerName: 'user'
        }
    }

}
