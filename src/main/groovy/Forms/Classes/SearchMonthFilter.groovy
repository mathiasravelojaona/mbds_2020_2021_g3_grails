package Forms.Classes

import com.mbds.grails.UtilService

class SearchMonthFilter {

    private Integer year
    public static final SearchMonthFilter defaultValue = new SearchMonthFilter(UtilService.getCurrentYear())

    void setYear(String year){
        try{
            this.year = Integer.valueOf(year)
        }
        catch (Exception e){
            this.year = null
        }
    }
    Integer getYear(){
        return this.year
    }
    SearchMonthFilter(String year){
        this.setYear(year)
    }
    SearchMonthFilter(Integer year){
        this.year = year
    }
}
