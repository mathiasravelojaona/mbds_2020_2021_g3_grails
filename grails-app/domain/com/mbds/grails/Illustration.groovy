package com.mbds.grails

class Illustration {

    // On va utiliser le filename pour construire deux chemins
    // URL = annonces.illustrations.url -> Adresse externe + chemin vers les images + filename
    // Path = annonces.illustrations.path -> Adresse interne + chemin vers les images + filename
    String filename
    Date dateCreated = new Date()
    Date lastUpdated = new Date()

    static belongsTo = [annonce: Annonce]
    static fetchMode = [annonce: 'eager']

    static mapping = {
        autoTimestamp false
    }
    static constraints = {
        filename    nullable: false, blank: false
        dateCreated nullable: false
        lastUpdated nullable: false
    }
}
