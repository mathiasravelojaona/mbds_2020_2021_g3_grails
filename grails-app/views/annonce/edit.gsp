<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <asset:stylesheet rel="stylesheet" href="annonces/annonces.css" />
    <style>
    .form-group{
        padding: 15px;
    }
    </style>
</head>

<body>
<div id="edit-annonce" class="content scaffold-edit" role="main">
    <div class="card">
        <div class="card-header">
            <h2 style="margin-bottom: 0;">Edition de l'annonce numéro ${annonce.id}</h2>
        </div>
        <div class="body">
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:form controller="annonce" action="update" id="${annonce.id}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                                <label for="titre">Titre <span class="required-indicator">*</span></label>
                                <input class="form-control" name="title" value="${annonce.title}" id="titre"/>
                                <g:renderErrors bean="${annonce}" as="list" field="title"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="price">Prix($) <span class="required-indicator">*</span></label>
                            <input type="number decimal" name="price" class="form-control" value="${annonce.price}" id="price"/>
                            <g:renderErrors bean="${annonce}" as="list" field="price"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="author">Auteur <span class="required-indicator">*</span></label>
                            <select class="form-control" id="author" name="Authorid">
                                <g:each in="${userList}" var="user">
                                    <g:if test="${user.id.toString().equals(annonce.idAuthor)}">
                                        <option value="${user.id}" selected>${user.username}</option>
                                    </g:if>
                                    <g:if test="${!user.id.toString().equals(annonce.idAuthor)}">
                                        <option value="${user.id}">${user.username}</option>
                                    </g:if>
                                </g:each>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea id="description" name="description" class="form-control">
                                ${raw(annonce.description)}
                            </textarea>
                            <g:renderErrors bean="${annonce}" as="list" field="description"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button style="margin: 15px;" class="btn btn-primary">
                            Mise à jour
                        </button>
                    </div>
                </div>
                <div class="row">
                </div>
            </g:form>
            <sec:ifAllGranted roles='ROLE_ADMIN'>
                <div class="container">
                        <div class="card">
                            <div class="card-header">
                                <h2 style="text-align: center;">
                                    <i class="fas fa-images" style="margin-right: 15px;"></i>Ajouter une nouvelle image
                                </h2>
                            </div>
                            <div class="card-body">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <span>Choisir un fichier</span>
                                        </div>
                                        <div class="col-sm-6">
                                            <input class="form-control" type="file" name="importFile" id="importFile" />
                                        </div>
                                        <div class="col-sm-6">
                                            <button class="btn btn-primary" type="button" id="btnUpload">Téléverser</button>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </sec:ifAllGranted>
            <div class="gallery" style="margin:20px;overflow: auto;" v-cloak>
                <div class="row" style="width:300%;overflow: auto;">
                    <div class="col-md-2" v-for="illustration in illustrations">
                        <div illustration
                             v-on:click="deleteImage(illustration.id)"
                             v-bind:style="{ backgroundImage: 'url(' + illustration.fileName + ')'}"
                             class="illustration">
                            <sec:ifAllGranted roles='ROLE_ADMIN'>
                             <div class="round-button">
                                <i class="fas fa-trash"></i>
                             </div>
                            </sec:ifAllGranted>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<footer>
    <content tag="javascript">
        <script>
            var id = ${annonce.id};
            var basePath = '/projet/assets/';
            console.log(basePath);
        </script>
        <asset:javascript src="annonces/annonces_edit.js" />
    </content>
</footer>
</html>
