package com.mbds.grails

import grails.gorm.transactions.Transactional

import java.text.SimpleDateFormat

@Transactional
class UtilService {
    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy")
    String encodeString(String texte) {
        return URLEncoder.encode(texte, "UTF-8")
    }
    static int getCurrentYear(){
        return 1900 + new Date().getYear()
    }
    static int getPageNumbers(int countnumber,int perpages) {
        int pages_temp = countnumber % perpages
        int resultat = countnumber / perpages
        if(pages_temp != 0){
            return resultat + 1
        }
        return resultat
    }
}
