<h1>Projet grails leCoincoin Groupe 3 Madagascar 2021</h1>
<h1>Membres du groupe : </h1>
 <ul>
	        <li>Numéro 31 RAMANANTSALAMA Anthony Tiana </li>
	        <li>Numéro 41 RANDRIANARISOA Narindra</li>
	        <li>Numéro 47 RASOANAIVO Ny Toky Andi</li>
			<li>Numéro 53 RAVELOJAONA Tokiniaina Mathias</li>
	        <li>Numéro 24 RAKOTOBE Holiarisoa Vetso Nombana</li>
	    </ul>
<h1>Fonctionnalités de base :</h1>
	<h3>1) Connexion et déconnexion au backend -> accès pour les admin et moderateur</h3>
	<h3>2) CRUD annonces</h3>
	<h3>3) CRUD utilisateurs</h3>
	<h3>4) Gestion de la sécurité -> admin peut tout faire et moderateur peut tout modifier mais ne peut pas créer ni 
supprimer</h3>
	<h3>5) Ergonomie correcte avec menu, cacher les casts, affichage des images</h3>
	<h3>6) API Rest avec une collection Postman avec les méthodes GET/ POST/ PUT/ PATCH/ DELETE pour les utilisateurs et 
les annonces avec les entêtes JSON et XML</h3>

<h1>Fonctionnalités ajoutées :</h1>
	<h3>1) Recherche multicritères sur les utilisateurs</h3>
	    <ul>
	        <li>Recherche sur leur nom</li>
	        <li>Recherche sur le statut de compte désactivé ou non</li>
	        <li>Possibilité de modifier directement le statut d'un utilisateur depuis la liste des utilisateurs</li>
	    </ul>
	<h3>2) Recherche pour pouvoir filtrer les annonces sur leur titre / description</h3>
	<h3>3)Conception et développement d'un tableau de bord</h3>
	    <ul>
	        <li>Statistiques sur le nombre d'articles créés</li>
	        <li>Statistiques sur le nombre d'utilisateurs existants</li>
	        <li>Statistiques sur le nombre d'images créés</li>
	        <li>Graphe sur le nombre d'articles produits par intervalle de jours avec fitre et par années</li>
	        <li>Graphe sur le nombre d'articles produits par mois durant les années</li>
	        <li>Graphe sur le nombre d'articles produits par jours de la semaine durant les années avec possibilité de filtrer par nois</li>
	        <li>Graphe sur les auteurs qui produisent le plus d'articles avec possibilité de choisir le mois de l'année , et le nombre d'auteurs à afficher</li>
	    </ul>
	<h3>4)Ecran gallerie</h3>
	    <ul>
	        <li>Gestion avançée et regroupement des illustrations</li>
	        <li>Possibilité de basculer à la fiche de l'annonce correspondante</li>
	        <li>Possibilité d'effacer directement l'illustration</li>
	        <li>Recherche multicritère par auteurs , mot-clé ou possibilité de trier et choisir une rangée de date entre la date d'ajout de l'image et celle de l'annonce</li>
	    </ul>
	<h3>5)Import et export csv pour les utilisateurs </h3>
	    <ul>
	        <li>Import uniquement pour l'admin</li>
	        <li>fichier test pour l'import dans racine/testupload.csv</li>
	    </ul>
	<h3>6)Export CSV pour les annonces</h3>
	<h3>7)API Rest avec une collection Postman avec les méthodes GET/ POST/ DELETE pour les illustrations avec les entêtes JSON et XML</h3>
	<h3>8)Sécurité pour les différentes fonctions de l'API REST</h3>
	<h3>9)Intégration du cdn cloudinary à l'application afin de stocker les images dans le Cloud</h3>
	<h3>10) Déploiement sur Heroku à l'adresse http://grails13fikambanana.herokuapp.com/projet</h3>
	<h3> Compte de test : </h3>
	 <ul>
	        <li>Nom : admin</li>
	        <li>Mot de passe : password</li>
</ul>
    <h3>11)Ajout de plusieurs fonctionnalités de l'application ( comme l'ajout d'illustrations à une annonce)</h3>
    <h3>12)Gestion à part des illustrations sur la page d'édition des annonces avec support de multiples images pour une seule annonce , possibilité d'ajouter ou de supprimer les images</h3>
    <h2>Pour lancer le projet chez vous :</h2>
    <ul>
    <li>Ouvrez le projet avec IntelliJ Ultimate</li>
    <li>Faites "run" après la synchronisation des dépendances gradle</li>
    </ul>
