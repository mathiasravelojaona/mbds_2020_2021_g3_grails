package com.mbds.grails

import Forms.Classes.InsertAnnonceForm
import Forms.Classes.UpdateAnnonceForm
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import org.springframework.web.multipart.MultipartFile
import java.text.SimpleDateFormat
import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.SpringSecurityUtils

@Secured(['ROLE_ADMIN', 'ROLE_MODO'])
class AnnonceController {

    AnnonceService annonceService
    CustomAnnonceService customAnnonceService
    UserService userService
    def exportService
    ObjectMapperService objectMapperService


    def index() {
        String searchkey = params.search
        params.max = 10
        def annonces = customAnnonceService.list(params, searchkey)
        respond annonces.annonceList,
                model:[annonceCount: annonces.count,
                       annonceList: annonces.annonceList,
                       keyword_annonce: searchkey]
    }

    def show(Long id) {
        switch (response.contentType){
            case 'application/json':
                render annonceService.get(id) as JSON
                break
            default :
                respond annonceService.get(id), model:[baseUrl: grailsApplication.config.annonces.illustrations.url]
                break
        }
    }

    @Secured(['ROLE_ADMIN'])
    def create() {
        [annonce: new InsertAnnonceForm(price: "",title:"",description:"",idAuthor:""), userList: User.list()]
    }

    @Secured(['ROLE_ADMIN'])
    def save() {
        InsertAnnonceForm annonce = new InsertAnnonceForm()
        try {
            annonce.title = params.title
            annonce.description = params.description
            annonce.price = params.price
            annonce.idAuthor = params.Authorid
            MultipartFile picture = params.illustration
            customAnnonceService.SaveWithImage(annonce, picture)
            flash.message = "L'annonce a bien été ajoutée"
            return redirect(controller: "annonce", action: "index")
        }
        catch (ValidationException e) {
            respond annonce.errors, model: [annonce: annonce,userList: User.list()],view:'create'
            return
        }
        catch(Exception e){
            flash.message = e.message
            return redirect(controller: "annonce", action: "create")
        }
    }

    def edit(Long id) {
        Annonce annonceEntity = annonceService.get(id)
        if(!annonceEntity){
            notFound()
            return
        }
        UpdateAnnonceForm annonce = objectMapperService.map(annonceEntity)
        respond annonce , model: [annonce: annonce, userList: User.list()], view: 'edit'
    }

    def update() {
        def annonce = null
        def annonceForm = new UpdateAnnonceForm()
        try {
            annonceForm.title = params.title
            annonceForm.description = params.description
            annonceForm.price = params.price
            annonceForm.id = params.id
            annonceForm.idAuthor = params.Authorid
            if(!annonceForm.validate()){
                throw new ValidationException('Une erreur est survenue',annonceForm.errors)
            }
            annonce =  this.customAnnonceService.updateAnnonce(annonceForm)
            flash.message = "L'annonce a bien été modifiée"
            redirect annonce
        }
        catch (ValidationException e) {
            respond e.errors,model: [annonce: annonceForm,userList: User.list()], view:'edit'
            return
        }
        catch (Exception e){
            flash.message = e.message
            redirect action: 'edit',params: [id: params.id]
        }
    }

    @Secured(['ROLE_ADMIN'])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }
        try{
            annonceService.delete(id)
            flash.message = "L'annonce a été supprimée"
            redirect action:"index", method:"GET"
        }
        catch (Exception e){
            flash.message = e.message
            redirect action:"index", method:"GET"
        }
    }

    def export(){
        params.exportFormat = "excel"
        params.extension = "xls"

        if(params.exportFormat && params.exportFormat != "html") {
            def annonces = customAnnonceService.findAll()

            def result = []
            annonces.each {a ->
                def map = [:]
                map.put("title", a.title)
                map.put("description", a.description)
                map.put("dateCreated", a.dateCreated)
                map.put("price", a.price)
                map.put("author", a.author.username)

                result.add(map)
            }

            response.contentType = grailsApplication.config.grails.mime.types[params.exportFormat]
            response.setHeader("Content-disposition", "attachment; filename=annonces-"+new SimpleDateFormat("ddMMyyy").format(new Date())+".xls")
            List fields = [
                    "title",
                    "description",
                    "dateCreated",
                    "price",
                    "author",
            ]
            Map labels = ["titre": "Titre", "Description": "description", "dateCreated": "Date de création", "price": "Prix ("+'$'+")", "author": "Auteur"]
            Map formatters = [:]
            Map parameters = [:]

            exportService.export(params.exportFormat, response.outputStream, result, fields, labels, formatters, parameters)
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'annonce.label', default: 'Annonce'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
