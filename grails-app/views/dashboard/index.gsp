<%--
  Created by IntelliJ IDEA.
  User: john
  Date: 19/03/2021
  Time: 15:27
--%>

<%@ page import="com.mbds.grails.UtilService" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main" />
    <asset:stylesheet rel="stylesheet" href="dashboard/dashboard.css" />
</head>

<body>
<div id="dashboard" class="content scaffold-edit">
    <div class="card" v-cloak>
        <div class="card-header">
            <h1>
                Statistiques du ${UtilService.simpleDateFormat.format(current_date)}
            </h1>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header" style="background: cornflowerblue; color:white;">
                            <h3>Articles</h3>
                        </div>
                        <div class="card-body">
                            <div class="dash-card">
                                    <i class="fas fa-book"></i>
                                    <span>[%nombre_articles%]</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header" style="background: deepskyblue; color: white;">
                            <h3>Utilisateurs</h3>
                        </div>
                        <div class="card-body">
                            <div class="dash-card" style="background: deepskyblue">
                                <i class="fas fa-user"></i>
                                <span>[%nombre_utilisateurs%]</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header" style="background: royalblue; color: white;">
                            <h3>Images ajoutées</h3>
                        </div>
                        <div class="card-body">
                            <div class="dash-card" style="background: royalblue">
                                <i class="fas fa-images"></i>
                                <span>[%nombre_illustrations%]</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" style="padding-bottom: 25px;">
                            <div class="card-header" style="background: royalblue; color: white;">
                                <h3>Nombre d'articles par jour</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="month_search"
                                             style="width:100%;height:100%;flex-direction: column;">
                                            <v-date-picker
                                                    is-range
                                                    v-model="filters.articles_days.dates"  />
                                        </div>
                                        <button style="width: 100%;border-radius: 4px;margin-bottom: 3px;"
                                                v-bind:disabled="!hasDate"
                                                v-on:click="filterByDate()"
                                                class="btn btn-outline-primary">Filtrer</button>
                                        <button style="width: 100%;border-radius: 4px;"
                                                v-bind:disabled="!hasDate"
                                                v-on:click="deleteIntervalFilter()"
                                                class="btn btn-outline-secondary">Supprimer le filtre</button>
                                    </div>
                                    <div class="col-md-8">
                                        <div id="article_jour" class="chart-affichage"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                        <div class="card-header" style="background: royalblue; color: white;">
                            <h3>Nombre d'articles par mois</h3>
                        </div>
                        <div class="card-body">
                            <div class="month_search">
                                <i class="fas fa-arrow-left" v-on:click="reduceYear()"></i>
                                <h4>[%filters.articles_month.year%]</h4>
                                <i class="fas fa-arrow-right" v-on:click="addYear()"></i>
                            </div>
                            <div id="article_mois" class="chart-affichage"></div>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header" style="background: royalblue; color: white;">
                                <h3>Nombre d'articles par jours de la semaine</h3>
                            </div>
                            <div class="card-body">
                                <div class="month_search">
                                    <i class="fas fa-arrow-left" v-on:click="reduceYearDOW()"></i>
                                    <h4>[%filters.DOW.year%]</h4>
                                    <i class="fas fa-arrow-right" v-on:click="addYearDOW()"></i>
                                </div>
                                <select v-on:change="refreshDOW()" v-model="filters.DOW.month" style="width:100%;margin-bottom: 20px;">
                                    <option value="all">Tous les mois</option>
                                    <option v-bind:value="month.key"
                                            v-for="month in months">
                                        [%month.value%]
                                    </option>
                                </select>
                                <div id="article_dow" class="chart-affichage"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header" style="background: royalblue; color: white;">
                                <h3>Nombre d'articles par utilisateur (Top [%filters.DOW.month%])</h3>
                            </div>
                            <div class="card-body">
                                <div class="filter-top">
                                    <select v-on:change="refreshTop()" v-model="filters.top.month">
                                        <option value="all">Tous les mois</option>
                                        <option v-bind:value="month.key"
                                                v-for="month in months">
                                            [%month.value%]
                                        </option>
                                    </select>
                                    <select v-on:change="refreshTop()" v-model="filters.top.top">
                                        <option value="5">Top 5</option>
                                        <option value="10">Top 10</option>
                                        <option value="15">Top 15</option>
                                    </select>
                                </div>
                                <div id="article_utilisateur" class="chart-affichage"></div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
</body>
<footer>
    <content tag="javascript">
        <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
        <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
        <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
        <asset:javascript src="dashboard/dashboard.js" />
    </content>
</footer>
</html>