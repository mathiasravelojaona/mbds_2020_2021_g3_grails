package projet_itu_2020_2021

import com.mbds.grails.Annonce
import com.mbds.grails.Illustration
import com.mbds.grails.User
import grails.converters.JSON

import java.text.SimpleDateFormat

class Marshaller {
    static String internal = "internal"
    static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy")
    static init(){
            JSON.registerObjectMarshaller(Illustration) { Illustration illustration ->
                return [
                        'id': illustration.id,
                        'idAnnonce': illustration.annonce.id,
                        'fileName': illustration.filename,
                        'date': simpleDateFormat.format(illustration.dateCreated),
                        'titre': illustration.annonce.title,
                        'dateAnnonce': simpleDateFormat.format(illustration.annonce.dateCreated),
                        'prix': illustration.annonce.price,
                        'auteur': illustration.annonce.author.username,
                ]
            }
            JSON.registerObjectMarshaller(Annonce) { Annonce annonce ->
                return [
                        'id':annonce.id,
                        'title':annonce.title,
                        'description':annonce.description,
                        'price':annonce.price,
                        'createdAt': simpleDateFormat.format(annonce.dateCreated),
                        'auteur': annonce.author.username,
                        'illustrations': annonce.illustrations
                ]
            }
            JSON.registerObjectMarshaller(User) { User user ->
                return [
                     'id': user.id,
                     'name': user.username,
                     'enabled':user.enabled,
                    'accountLocked' : user.accountLocked,
                    'accountExpired' : user.accountExpired,
                    'passwordExpired' : user.passwordExpired
                ]
            }
    }
}
