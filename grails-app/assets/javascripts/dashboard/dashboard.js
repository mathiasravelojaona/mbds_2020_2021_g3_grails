const getData = function(){
    return new Promise((resolve,reject) => {
        $.ajax({
            url: `/projet/dashboard/statistiques`,
            type: "get",
            contentType: 'application/json',
            dataType: 'json',
            success: function(data) {
                resolve(data);
            },
            error: function(xhr){
                reject(xhr);
            }
        });
    });
};
const statMonths = function(year){
    return new Promise((resolve,reject) => {
        $.ajax({
            url: `/projet/dashboard/articlesParMois?year=${year}`,
            type: "get",
            contentType: 'application/json',
            dataType: 'json',
            success: function(data) {
                resolve(data);
            },
            error: function(xhr){
                reject(xhr);
            }
        });
    });
};
const statDOW = function(year,month){
    return new Promise((resolve,reject) => {
        $.ajax({
            url: `/projet/dashboard/articlesParDow?year=${year}&month=${month}`,
            type: "get",
            contentType: 'application/json',
            dataType: 'json',
            success: function(data) {
                resolve(data);
            },
            error: function(xhr){
                reject(xhr);
            }
        });
    });
};
const statDays = function(d1,d2){
    return new Promise((resolve,reject) => {
        $.ajax({
            url: `/projet/dashboard/articlesParJour?d1=${d1}&d2=${d2}`,
            type: "get",
            contentType: 'application/json',
            dataType: 'json',
            success: function(data) {
                resolve(data);
            },
            error: function(xhr){
                reject(xhr);
            }
        });
    });
};
const topAuthors = function(top,month){
    return new Promise((resolve,reject) => {
        $.ajax({
            url: `/projet/dashboard/topAuthors?top=${top}&month=${month}`,
            type: "get",
            contentType: 'application/json',
            dataType: 'json',
            success: function(data) {
                resolve(data);
            },
            error: function(xhr){
                reject(xhr);
            }
        });
    });
};
const months = {
  1:"Janvier",
  2:"Février",
  3:"Mars",
  4:"Avril",
  5:"Mai",
  6:"Juin",
  7:"Juillet",
  8:"Août",
  9:"Septembre",
  10:"Octobre",
  11:"Novembre",
  12:"Décembre",
};
const monthArray = Object.keys(months).map(key => ({key, value: months[key]}));
const dow = {
  2: "Lundi",
  3: "Mardi",
  4: "Mercredi",
  5: "Jeudi",
  6: "Vendredi",
  7: "Samedi",
  1: "Dimanche",
};

Vue.use(VueLoading);
Vue.use(Toasted);
Vue.component('loading', VueLoading);
let application = new Vue({
    el: document.getElementById('dashboard'),
    delimiters: ['[%','%]'],
    data: function() {
        return {
            charts: {
              articles_jour: null,
              articles_mois: null,
              articles_dow: null,
              articles_utilisateurs: null,
            },
            filters: {
                articles_days: {
                    dates: {
                        start: null,
                        end: null
                    },
                },
                articles_month: {
                  year: 2021,
                },
                DOW: {
                  year: 2021,
                  month: "all",
                },
                top: {
                  top: 5,
                  month: "all",
                }
            },
            nombre_articles: 0,
            nombre_illustrations: 0,
            nombre_utilisateurs: 0,
            nombre_articles_jour: [],
            nombre_articles_mois: [],
            nombre_articles_dow: [],
            nombre_articles_utilisateurs: [],
            top_users: [],
            months: monthArray,
        }
    },
    computed:{
        hasDate(){
          return this.filters.articles_days.dates.start;
        },
    },
    methods:{
        initCharts(){
            const that = this;
            am4core.ready(function() {
                am4core.useTheme(am4themes_animated);
                that.bindArticlesDaysChart();
                that.bindArticlesMonthsChart();
                that.bindArticlesDOW();
                that.bindArticlesUser();
            });
        },
        getLoader(){
            return Vue.$loading.show({
                container: this.$refs.loadingContainer,
                canCancel: true, // default false
                onCancel: this.yourCallbackMethod,
                color: '#000000',
                loader: 'spinner',
                width: 64,
                height: 64,
                backgroundColor: '#ffffff',
                opacity: 0.5,
                zIndex: 999,
            });
        },
        async refreshMonths(){
            const loader = this.getLoader();
            try{
                const data = await statMonths(this.filters.articles_month.year);
                this.nombre_articles_mois = this.full_month(data.stats);
                console.log(this.nombre_articles_mois);
                this.bindArticlesMonthsChart();
            }
            catch (e) {
                console.log(e);
                let myToast = this.$toasted.show("Une erreur de connexion est survenue");
                myToast.goAway(1500);
            }
            finally {
                loader.hide();
            }
        },
        async refreshDOW(){
            const loader = this.getLoader();
            try{
                const data = await statDOW(this.filters.DOW.year,this.filters.DOW.month);
                this.nombre_articles_dow = this.full_month(data.stats);
                this.bindArticlesDOW();
            }
            catch (e) {
                console.log(e);
                let myToast = this.$toasted.show("Une erreur de connexion est survenue");
                myToast.goAway(1500);
            }
            finally {
                loader.hide();
            }
        },
        reduceYear() {
            const year = this.filters.articles_month.year - 1;
            if(year < 1960){
                let myToast = this.$toasted.show("L'année ne doit pas être supérieure à 2050");
                myToast.goAway(1500);
                return;
            }
            this.filters.articles_month.year-= 1;
            this.refreshMonths();
        },
        addYear(){
            const year = this.filters.articles_month.year + 1;
            this.filters.articles_month.year+= 1;
            this.refreshMonths();
        },
        reduceYearDOW() {
            const year = this.filters.DOW.year - 1;
            if(year < 1960){
                let myToast = this.$toasted.show("L'année ne doit pas être supérieure à 2050");
                myToast.goAway(1500);
                return;
            }
            this.filters.DOW.year-= 1;
            this.refreshDOW();
        },
        addYearDOW(){
            const year = this.filters.DOW.year + 1;
            this.filters.DOW.year+= 1;
            this.refreshDOW();
        },
        getCleanDataArticlesDays(dataSource){
            const tableau = [];
            dataSource.forEach((element)=>{
                const obj = {};
                obj.date = moment(element[0],'DD-MM-YYYY').toDate();
                obj.valeur = element[1];
                tableau.push(obj);
            });
            return tableau;
        },
        getCleanDataArticlesMonths(dataSource){
            const tableau = [];
            dataSource.forEach((element)=>{
                const obj = {};
                obj.date = months[element[0]];
                obj.valeur = element[1];
                tableau.push(obj);
            });
            return tableau;
        },
        getCleanDataDOW(dataSource){
            const tableau = [];
            dataSource.forEach((element)=>{
                const obj = {};
                obj.date = dow[element[0]];
                obj.valeur = element[1];
                tableau.push(obj);
            });
            return tableau;
        },
        getCleanDataArticleUser(dataSource){
            const tableau = [];
            dataSource.forEach((element)=>{
                const obj = {};
                obj.date = element[0].name;
                obj.valeur = element[1];
                tableau.push(obj);
            });
            return tableau;
        },
        bindArticlesMonthsChart(){
            if(this.charts.articles_mois){
                this.charts.articles_mois.data = this.getCleanDataArticlesMonths(this.nombre_articles_mois);
                return;
            }
            this.charts.articles_mois = am4core.create("article_mois", am4charts.XYChart);
            this.charts.articles_mois.data = this.getCleanDataArticlesMonths(this.nombre_articles_mois);
            var categoryAxis = this.charts.articles_mois.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "date";
            categoryAxis.renderer.minGridDistance = 60;
            console.log(this.charts.articles_mois.data);
            var valueAxis = this.charts.articles_mois.yAxes.push(new am4charts.ValueAxis());
            valueAxis.min = 0;
            var series = this.charts.articles_mois.series.push(new am4charts.ColumnSeries());
            series.dataFields.categoryX = "date";
            series.dataFields.valueY = "valeur";
            series.tooltipText = "{valueY.value}";
            series.columns.template.strokeOpacity = 0;
            series.columns.template.column.cornerRadiusTopRight = 10;
            series.columns.template.column.cornerRadiusTopLeft = 10;
            var labelBullet = series.bullets.push(new am4charts.LabelBullet());
            labelBullet.label.verticalCenter = "bottom";
            labelBullet.label.dy = -10;
            labelBullet.label.text = "{values.valueY.workingValue.formatNumber('#.')}";
            this.charts.articles_mois.zoomOutButton.disabled = true;
            const that = this;
// as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
            series.columns.template.adapter.add("fill", function (fill, target) {
                return that.charts.articles_mois.colors.getIndex(target.dataItem.index);
            });
        },
        bindArticlesDaysChart(){
            const that = this;
            this.charts.articles_jour = am4core.create("article_jour", am4charts.XYChart);
            this.charts.articles_jour.data = this.getCleanDataArticlesDays(this.nombre_articles_jour);
            var dateAxis = this.charts.articles_jour.xAxes.push(new am4charts.DateAxis());
            var valueAxis = this.charts.articles_jour.yAxes.push(new am4charts.ValueAxis());
            valueAxis.min = 0;
            var series = this.charts.articles_jour.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = "valeur";
            series.dataFields.dateX = "date";
            series.strokeWidth = 1;
            series.minBulletDistance = 10;
            series.tooltipText = "Articles";
            series.fillOpacity = 0.1;
            series.tooltip.pointerOrientation = "vertical";
            series.tooltip.background.cornerRadius = 20;
            series.tooltip.background.fillOpacity = 0.5;
            series.tooltip.label.padding(12, 12, 12, 12);

            var seriesRange = dateAxis.createSeriesRange(series);
            seriesRange.contents.strokeDasharray = "2,3";
            seriesRange.contents.stroke = this.charts.articles_jour.colors.getIndex(8);
            seriesRange.contents.strokeWidth = 1;

            var pattern = new am4core.LinePattern();
            pattern.rotation = -45;
            pattern.stroke = seriesRange.contents.stroke;
            pattern.width = 1000;
            pattern.height = 1000;
            pattern.gap = 6;
            seriesRange.contents.fill = pattern;
            seriesRange.contents.fillOpacity = 0.5;
        },
        bindArticlesDOW(){
            if(this.charts.articles_dow){
                this.charts.articles_dow.data = this.getCleanDataDOW(this.nombre_articles_dow);
                return;
            }
            this.charts.articles_dow = am4core.create("article_dow", am4charts.XYChart);
            this.charts.articles_dow.data = this.getCleanDataDOW(this.nombre_articles_dow);
            var categoryAxis = this.charts.articles_dow.xAxes.push(new am4charts.CategoryAxis());
            //this.charts.articles_mois.renderer.grid.template.location = 0;
            categoryAxis.dataFields.category = "date";
            categoryAxis.renderer.minGridDistance = 60;
            //categoryAxis.renderer.inversed = true;
            categoryAxis.renderer.grid.template.disabled = true;
            var valueAxis = this.charts.articles_dow.yAxes.push(new am4charts.ValueAxis());
            var series = this.charts.articles_dow.series.push(new am4charts.ColumnSeries());
            series.dataFields.categoryX = "date";
            series.dataFields.valueY = "valeur";
            valueAxis.min = 0;
            series.tooltipText = "{valueY.value}";
            series.columns.template.strokeOpacity = 0;
            series.columns.template.column.cornerRadiusTopRight = 10;
            series.columns.template.column.cornerRadiusTopLeft = 10;
            var labelBullet = series.bullets.push(new am4charts.LabelBullet());
            labelBullet.label.verticalCenter = "bottom";
            labelBullet.label.dy = -10;
            labelBullet.label.text = "{values.valueY.workingValue.formatNumber('#.')}";
            this.charts.articles_dow.zoomOutButton.disabled = true;
            const that = this;
// as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
            series.columns.template.adapter.add("fill", function (fill, target) {
                return that.charts.articles_dow.colors.getIndex(target.dataItem.index);
            });
        },
        bindArticlesUser(){
            this.charts.articles_utilisateurs = am4core.create("article_utilisateur", am4charts.XYChart);
            this.charts.articles_utilisateurs.data = this.getCleanDataArticleUser(this.nombre_articles_utilisateurs);
            var categoryAxis = this.charts.articles_utilisateurs.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "date";
            categoryAxis.renderer.labels.template.rotation = 260;
            categoryAxis.renderer.minGridDistance = 60;
            //categoryAxis.renderer.inversed = true;
            categoryAxis.renderer.grid.template.disabled = true;
            var valueAxis = this.charts.articles_utilisateurs.yAxes.push(new am4charts.ValueAxis());
            var series = this.charts.articles_utilisateurs.series.push(new am4charts.ColumnSeries());
            series.dataFields.categoryX = "date";
            series.dataFields.valueY = "valeur";
            series.tooltipText = "{valueY.value}";
            series.columns.template.strokeOpacity = 0;
            series.columns.template.column.cornerRadiusTopRight = 10;
            series.columns.template.column.cornerRadiusTopLeft = 10;
            var labelBullet = series.bullets.push(new am4charts.LabelBullet());
            labelBullet.label.verticalCenter = "bottom";
            labelBullet.label.dy = -10;
            labelBullet.label.text = "{values.valueY.workingValue.formatNumber('#.')}";
            this.charts.articles_utilisateurs.zoomOutButton.disabled = true;
            const that = this;
// as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
            series.columns.template.adapter.add("fill", function (fill, target) {
                return that.charts.articles_utilisateurs.colors.getIndex(target.dataItem.index);
            });
            categoryAxis.sortBySeries = series;
        },
        async init(){
            let loader = this.getLoader();
            try{
                const data = await getData();
                this.nombre_articles = data.nombre_articles;
                this.nombre_illustrations = data.nombre_illustrations;
                this.nombre_utilisateurs = data.nombre_utilisateurs;
                this.nombre_articles_jour = data.nombre_articles_par_jour;
                this.nombre_articles_mois = this.full_month(data.nombre_articles_par_mois);
                this.nombre_articles_dow = this.full_dow(data.nombre_articles_par_jour_semaine);
                this.nombre_articles_utilisateurs = data.top_auteurs;
                this.initCharts();
            }
            catch (e) {
                let myToast = this.$toasted.show(e.message);
                myToast.goAway(1500);
                console.log(e);
            }
            finally {
                loader.hide();
            }
        },
        full_month(data){
          const presentMonths = data.map(row => row[0]);
          Object.keys(months).forEach((row,index) => {
              if(presentMonths.indexOf(index + 1) === -1){
                  data.push([index + 1, 0]);
              }
          });
            data = data.sort(function(a,b){
                return a[0] - b[0];
            });
            return data;
        },
        full_dow(data){
            const presenDow = data.map(row => row[0]);
            Object.keys(months).forEach((row,index) => {
                if(presenDow.indexOf(index + 1) === -1){
                    data.push([index + 1, 0]);
                }
            });
            data = data.sort(function(a,b){
                return a[0] - b[0];
            });
            return data;
        },
        async refreshDays(){
            const loader = this.getLoader();
            try{
                const d1 = this.filters.articles_days.dates.start ?  moment(this.filters.articles_days.dates.start).format('DD/MM/YYYY') : null;
                const d2  = this.filters.articles_days.dates.end ? moment(this.filters.articles_days.dates.end).format('DD/MM/YYYY') : null;
                const data = await statDays(d1,d2);
                this.nombre_articles_jour = data.stats;
                this.bindArticlesDaysChart();
            }
            catch (e) {
                console.log(e);
                let myToast = this.$toasted.show("Une erreur de connexion est survenue");
                myToast.goAway(1500);
            }
            finally {
                loader.hide();
            }
        },
        filterByDate(){
            this.refreshDays();
        },
        deleteIntervalFilter(){
            Vue.set(this.filters.articles_days,'dates',{
               start: null,
               end: null,
            });
            console.log(this.filters.articles_days.dates);
            this.refreshDays();
        },
        async refreshTop(){
            const loader = this.getLoader();
            try{
                const data = await topAuthors(this.filters.top.top,this.filters.top.month);
                this.nombre_articles_utilisateurs = data.stats;
                this.bindArticlesUser();
            }
            catch (e) {
                console.log(e);
                let myToast = this.$toasted.show("Une erreur de connexion est survenue");
                myToast.goAway(1500);
            }
            finally {
                loader.hide();
            }
        },
    },
    async mounted(){
        this.init();
    },
});