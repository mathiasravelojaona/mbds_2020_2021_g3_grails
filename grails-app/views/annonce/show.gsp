<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
        <asset:stylesheet src="annonces.css" />
    </head>
    <body>
        <div class="card">
            <div class="card-header space-between">
                <h2 style="margin-bottom: 0;">Fiche de l'annonce numéro ${annonce.id}</h2>
                <nav>
                    <g:link controller="annonce"
                            action="edit" id="${annonce.id}">
                        <div class="round-button">
                            <i class="fas fa-edit"></i>
                        </div>
                    </g:link>
                </nav>
            </div>
            <div class="card-body">
                <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12">
                        <table fiche_annonce class="table">
                            <thead class="table-bordered">
                                <tr>
                                    <td class="title">Titre</td>
                                    <td class="text">${annonce.title}</td>
                                </tr>
                                <tr>
                                    <td class="title">Prix</td>
                                    <td class="text"><g:formatNumber number="${annonce.price}" />$</td>
                                </tr>
                                <tr>
                                    <td class="title">Auteur</td>
                                    <td class="text">
                                        <g:link controller="user" action="show" id="${this.annonce.authorId}">
                                            ${annonce.author.username}
                                        </g:link>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="title">Description</td>
                                    <td class="text">
                                        ${raw(annonce.description)}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="title">
                                        Date de création
                                    </td>
                                    <td class="text">
                                        <g:formatDate format="dd/MM/yyyy" date="${annonce.dateCreated}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle;" class="title">Images</td>
                                    <td style="overflow: auto;">
                                        <div class="row" style="width:300%;overflow: auto;">
                                            <g:each
                                                    in="${annonce.illustrations}"
                                                    var="image"
                                                    status="i">
                                                <div class="col-md-2">
                                                    <div illustration
                                                         style="background-image:url('${image.filename}')"
                                                         class="illustration">
                                                    </div>
                                                </div>
                                            </g:each>
                                        </div>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>

                <sec:ifAllGranted roles='ROLE_ADMIN'>
                    <g:form resource="${this.annonce}" method="DELETE">
                        <div class="row">
                            <div class="col-md-12">
                                <g:link controller="annonce" action="delete" id="${this.annonce.authorId}">
                                    <button class="btn btn-danger" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'En êtes-vous sûr?')}');">Supprimer</button>
                                </g:link>
                            </div>
                        </div>
                    </g:form>
                </sec:ifAllGranted>
            </div>
        </div>
    </body>
</html>
