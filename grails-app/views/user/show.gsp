<%--
  Created by IntelliJ IDEA.
  User: Andi
  Date: 12/03/2021
  Time: 16:06
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
    <title>Profil Utilisateur</title>
</head>

<body>
<div class="card mb-4">
    <h6 class="card-header">Profil Utilisateur</h6>
    <div class="card-body">
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
            <div class="form-group">
                <label class="form-label">Nom d'utilisateur</label>
                <input type="text" class="form-control" placeholder="Email" value="${user.username}" name="username" disabled>
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" ${user.enabled ? "checked" : "" } name="enabled" disabled>
                    <span class="custom-control-label">Activé</span>
                </label>
            </div>
            <div class="form-group">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input"  ${user.accountExpired ? "checked" : "" } name="accountExpired" disabled>
                    <span class="custom-control-label">Compte expiré</span>
                </label>
            </div>
            <div class="form-group">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input"  ${user.accountLocked ? "checked" : "" } name="accountLocked" disabled>
                    <span class="custom-control-label">Compte vérrouillé</span>
                </label>
            </div>
            <div class="form-group">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input"  ${user.passwordExpired ? "checked" : "" } name="passwordExpired" disabled>
                    <span class="custom-control-label">Mot de passe expiré </span>
                </label>
            </div>
            <g:link controller="user" action="edit" id="${user.id}">
                <button type="button" class="btn btn-primary">Modifier</button>
            </g:link>
    </div>
</div>

</body>
</html>