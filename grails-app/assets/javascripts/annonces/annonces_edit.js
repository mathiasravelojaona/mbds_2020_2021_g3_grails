tinymce.init({
    selector: '#description'
});
const getIllustrations = function(){
    return new Promise((resolve,reject) => {
        $.ajax({
            url: `/projet/annonce/show/${id}`,
            type: "get",
            contentType: 'application/json',
            dataType: 'json',
            success: function(data) {
                resolve(data);
            },
            error: function(xhr){
                reject(xhr);
            }
        });
    });
};
const uploadFile = function(formdata){
    return new Promise((resolve,reject) => {
        $.ajax({
            url: `/projet/illustration/save?id=${id}`,
            data: formdata,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                resolve(data);
            },
            error: function(xhr){
                reject(xhr);
            }
        });
    });
};
const deleteIllustration = function(idIllustration){
    return new Promise((resolve,reject) => {
        $.ajax({
            url: `/projet/illustration/delete/${idIllustration}`,
            type: "delete",
            contentType: 'application/json',
            dataType: 'json',
            success: function(data) {
                resolve(data);
            },
            error: function(xhr){
                reject(xhr);
            }
        });
    });
};


Vue.use(VueLoading);
Vue.use(Toasted);
Vue.component('loading', VueLoading);
let application = new Vue({
    el: document.getElementById('edit-annonce'),
    delimiters: ['[%','%]'],
    data: function() {
        return {
            illustrations:[],
            basePath: basePath,
        }
    },
    methods:{
        getLoader(){
            return Vue.$loading.show({
                container: this.$refs.loadingContainer,
                canCancel: true, // default false
                onCancel: this.yourCallbackMethod,
                color: '#000000',
                loader: 'spinner',
                width: 64,
                height: 64,
                backgroundColor: '#ffffff',
                opacity: 0.5,
                zIndex: 999,
            });
        },
        async deleteImage(id){
            let loader = this.getLoader();
            try{
                const data = await deleteIllustration(id);
                let myToast = this.$toasted.show(data.message);
                myToast.goAway(1500);
                this.init();
            }
            catch (e) {
                let myToast = this.$toasted.show(e.message);
                myToast.goAway(1500);
                console.log(e);
            }
            finally {
                loader.hide();
            }
        },
        async uploadFile(){
            let loader = this.getLoader();
            try{
                if(!$("#importFile").get(0)){
                    let myToast = this.$toasted.show("Veuillez choisir une photo");
                    myToast.goAway(1500);
                }
                var files = $("#importFile").get(0).files;
                var formData = new FormData();
                if(files.length>0){
                    formData.append('importFile', files[0]);
                    let resultat = await uploadFile(formData);
                    let myToast = this.$toasted.show(resultat.message);
                    myToast.goAway(1500);
                    $('#importFile').val('');
                    this.init();
                }
            }
            catch (e) {
                console.log(e);
                let myToast = this.$toasted.show('Une erreur est survenue');
                myToast.goAway(1500);
            }
            finally {
                loader.hide();
            }
        },
        async init(){
            let loader = this.getLoader();
            try{
                const data = await getIllustrations();
                console.log(data);
                this.illustrations = data.illustrations;
            }
            catch (e) {
                let myToast = this.$toasted.show(e.message);
                myToast.goAway(1500);
                console.log(e);
            }
            finally {
                loader.hide();
            }
        },
    },
    async mounted(){
        const that = this;

        this.init();
        $(document).on("click", "#btnUpload", function () {
            that.uploadFile();
        });
    },
});