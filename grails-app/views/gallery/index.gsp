<%--
  Created by IntelliJ IDEA.
  User: macos
  Date: 2021-03-24
  Time: 17:04
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main" />
    <asset:stylesheet src="tags-css.css" />
    <asset:stylesheet src="gallery/gallery.css" />
</head>
<body>
    <div id="gallery" class="content" style="padding: 10px;background: white;" v-cloak>
        <div class="row" style="padding: 0;">
            <div class="col-md-9">
                <div class="auteurs-search">
                    <tags-input element-id="tags"
                                style="margin-bottom:25px;"
                                v-model="selectedTags"
                                placeholder="Auteurs"
                                v-on:tags-updated="onTagsUpdated"
                                v-on:change="onKeyUp"
                                v-bind:existing-tags="searchResults"
                                v-bind:typeahead="true">

                    </tags-input>
                    <button
                            v-on:click="filtrer_par_auteur()"
                            style="width: 100%;font-size: .8rem;padding: .6rem;margin-top: 10px;"
                            class="btn btn-lg btn-primary">
                        Filtrer
                    </button>
                    <div class="toolbox-button" style="max-width: 150px;" v-on:click="changeOrder()">
                        <i v-if="asc" style="color:white;"
                            class="fas fa-arrow-up"></i>
                        <i v-if="!asc" style="color:white;"
                            class="fas fa-arrow-down"></i>

                        <span v-if="asc">Ordre croissant</span>
                        <span v-if="!asc">Ordre décroissant</span>
                    </div>
                </div>
                <div class="row" style="margin: unset;padding: 0;">
                    <div v-for="image in liste"
                         style="background: antiquewhite;
                         padding-top: 3px;
                         margin-bottom: 10px;
                         border-radius: 5px;"
                         class="col-md-3">
                        <div class="image-card"  >
                            <div class="date-container"
                                 style="background: cornflowerblue;
                                 color: white;
                                 font-weight: bold;">
                                <i class="fas fa-calendar"></i>
                                <i class="fas fa-book"></i>
                                <span>[%image.dateAnnonce%]</span>
                            </div>
                            <div class="date-container"
                                 style="background: orange;
                                 color: white;
                                 font-weight: bold;">
                                <i class="fas fa-calendar"></i>
                                <i class="fas fa-image"></i>
                                <span>[%image.date%]</span>
                            </div>
                            <div class="row">
                                <div class="col col-md-10">
                                    <div v-bind:style="{ backgroundImage: 'url(' + image.fileName + ')' }"
                                         class="content"></div>
                                </div>
                                <div class="col col-md-2">
                                    <div class="action-right">
                                    <sec:ifAllGranted roles='ROLE_ADMIN'>
                                        <div
                                             v-on:click="removeIllustration(image.id)"
                                             class="action-btn"
                                             style="background: red;">
                                            <i class="fas fa-trash"></i>
                                        </div>
                                    </sec:ifAllGranted>
                                        <a class="action-btn" v-bind:href="'/projet/annonce/show/'+image.idAnnonce"
                                                 style="background: deepskyblue;">
                                                <i class="fas fa-eye"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-title">
                                <h4>[%image.titre%]</h4>
                            </div>
                            <div class="date-container"
                                 style="background: orangered;
                                 color: white;
                                 font-weight: bold;">
                                <i class="fas fa-user"></i>
                                <span>[%image.auteur%]</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-body-paginate"
                     v-if="pagenumbers!=0"
                     style="margin-top:20px;width:100%;">
                    <span class="limit-page" v-if="page!=1" v-on:click="goTo(1)">
                        <p style="color:black;">
                            1
                        </p>
                    </span>
                    <span class="temp-bascule" v-if="page!=1" v-on:click="goTo(page - 1)">
                        <p>PREV</p>
                    </span>
                    <div class="dot" v-if="page!=1">
                        <p>&bull;</p>
                    </div>
                    <span class="current-page">
                        <p>[%page%]</p>
                    </span>
                    <div class="dot" v-if="page!=pagenumbers">
                        <p>&bull;</p>
                    </div>
                    <span class="temp-bascule" v-if="page!=pagenumbers" v-on:click="goTo(page + 1)">
                        <p>Next</p>
                    </span>
                    <span class="limit-page"
                          v-on:click="goTo(pagenumbers)"
                          v-if="page!=pagenumbers">
                        <p style="color:black;">[%pagenumbers%]</p>
                    </span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="menu-right">
                    <div class="search-zone">
                        <div class="searchbar">
                            <i class="fas fa-search"></i>
                            <input class="search-input" v-model="keyword" type="text"/>
                        </div>
                        <div class="date-picker">
                            <div class="toolbox">
                                <h3>
                                    Mode de filtre des dates
                                </h3>
                                <div class="row">
                                    <div class="col-md-6">
                                            <div class="toolbox-button"
                                                 v-on:click="setGalleryDate(true)"
                                                 v-bind:class="{'active':galleryDate}">
                                                <span>Illustrations</span>
                                            </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="toolbox-button"
                                             v-on:click="setGalleryDate(false)"
                                             v-bind:class="{'active':!galleryDate}">
                                            <span>Annonces</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h3>Intervalle de création</h3>
                            <v-date-picker
                                    is-range
                                    v-model="range"  />
                        </div>
                        <button style="width: 100%;
                            font-size: .8rem;
                            padding: .6rem;
                            margin-top: 10px;"
                                v-if="hasDate"
                                v-on:click="deleteDateFilter()"
                                class="btn btn-secondary">
                            Supprimer les filtres
                        <button style="width: 100%;
                                font-size: .8rem;
                                padding: .6rem;
                                margin-top: 10px;"
                                v-on:click="filtrer_par_auteur()"
                                class="btn btn-primary">
                            Rechercher
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<footer>
    <content tag="javascript">
        <asset:javascript src="tags-js.js" />
        <asset:javascript src="gallery/gallery.js"/>
    </content>
</footer>
</html>