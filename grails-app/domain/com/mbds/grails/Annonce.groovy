package com.mbds.grails

class Annonce {

    String title
    String description
    Double price

    Date dateCreated = new Date()
    Date lastUpdated = new Date()
    String createdDayFormatted
    int month
    int dayOfWeek
    int year

    static hasMany = [illustrations: Illustration]
    static fetchMode = [author: 'eager']
    static belongsTo = [author: User]

    static constraints = {
        title       nullable: false, blank: false
        description nullable: false, blank: false
        price       min: 0D
        dateCreated nullable: false
        lastUpdated nullable: false
    }

    static mapping = {
        description type: 'text'
        autoTimestamp false
        createdDayFormatted formula: "FORMATDATETIME(date_created,'dd-MM-yyyy')"
        month formula: 'MONTH(date_created)'
        year formula: 'YEAR(date_created)'
        dayOfWeek formula: 'DAY_OF_WEEK(date_created)'
    }
}
