<g:set var="user" value="${request.getAttribute("current_user")}" />
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <link rel="stylesheet" href="https://unpkg.com/@egoist/snackbar/dist/snackbar.min.css"/>
    </head>
    <body>
        <div id="list-user" class="content scaffold-list" role="main">
            <h1> Liste des utilisateurs </h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <sec:ifAllGranted roles='ROLE_ADMIN'>
                <div class="export_button" style="display: flex; justify-content: space-between;align-items:center;
                background: white;padding: 5px;">
                    <g:uploadForm controller="user" action="upload" name="searchForm" class="tm-contact-form">
                        <div class="rs-select2--light rs-select2--md">
                            <input id="filecsv" name="filecsv" type="file"/>
                        </div>&nbsp
                        <button class="btn btn-lg btn-outline-primary" style="margin-top: 10px;">Import</button>
                    </g:uploadForm>
                    <div class="export_button" style="display: flex; justify-content: flex-end">
                        <g:link controller="user" action="export" class="btn btn-lg btn-outline-primary">Export</g:link>
                    </div>
                </div>
            </sec:ifAllGranted>
            <g:form controller="user"
                    method="get"
                    action="index"
                    class="form-row mb-1" style="box-shadow:none;background: white;margin-top: 21px;">
                <div class="form-group col-md-4">
                    <input type="text"
                           value="${text}"
                           name="searchkey"
                           class="form-control"
                           placeholder="Recherche">
                </div>
                <div class="form-group col-md-4">
                    <select class="custom-select" id="status" name="status">
                        <option value="" ${status == null ? 'selected': ''}>-- Selectionner status -- </option>
                        <option value="true" ${status == "true" ? 'selected': ''}>Actif</option>
                        <option value="false" ${status == "false" ? 'selected': ''}>Inactif</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-primary">Rechercher</button>
                </div>
            </g:form>
            </br>
            <table class="table table-bordered table">
                <thead>
                <tr>
                    <g:sortableColumn class="field_text" property="username"
                                      title="Nom d'utilisateur" />
                    <g:sortableColumn class="field_text" property="enabled"
                                      title="Status" />
                    <g:sortableColumn class="field_text" property="Action"
                                      title="Action"/>
                </tr>
                </thead>
                <tbody>
                <g:if test="${userCount <= 0}">
                    <tr>
                        <td>Aucun.</td>
                        <td>Aucun.</td>
                        <td>Aucun.</td>
                    </tr>
                </g:if>
                <g:else>
                    <g:each in="${userList}" var="bean" status="i">
                        <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                            <td class="field_text">
                                <g:link method="GET" resource="${bean}">
                                    <p class="field_text">${bean.username}</p>
                                </g:link>
                            </td>
                            <td>
                                <div class="form-check form-switch">
                                    <g:if test="${user.id != bean.id}">
                                        <g:if test="${bean.enabled == false}">
                                                <input name="checkbox-${bean.id}" class="form-check-input enable-check" type="checkbox" id="flexSwitchCheckChecked">
                                                <label name="label-${bean.id}" class="form-check-label" style="display: flex; justify-content: space-around" for="flexSwitchCheckChecked">
                                                    <span class="badge badge-danger">Inactif</span>
                                                </label>
                                        </g:if>
                                        <g:if test="${bean.enabled == true}">
                                                <input name="checkbox-${bean.id}" class="form-check-input enable-check" type="checkbox" id="flexSwitchCheckChecked" checked>
                                                <label name="label-${bean.id}" class="form-check-label" style="display: flex; justify-content: space-around" for="flexSwitchCheckChecked">
                                                    <span class="badge badge-success">
                                                        Actif
                                                    </span>
                                                </label>
                                        </g:if>
                                    </g:if>
                                </div>
                            </td>
                            <td>
                                <g:link controller="User" action="edit" params="[id: bean.id]" class="btn btn-primary">
                                    Modifier
                                </g:link>
                                <g:if test="${user.id != bean.id}">
                                    <sec:ifAllGranted roles='ROLE_ADMIN'>
                                        <g:link controller="user" action="delete" params="[id: bean.id]"
                                                class="btn btn-danger exampleModal">
                                            Supprimer
                                        </g:link>
                                    </sec:ifAllGranted>
                                </g:if>

                            </td>
                        </tr>
                    </g:each>
                </g:else>
                </tbody>
            </table>

            <g:if test="${userCount > 0}">
                <div class="pagination">
                    <g:paginate params="${['searchkey':text,'status':status]}" total="${userCount ?: 0}" />
                </div>
            </g:if>
        </div>
    </body>
    <footer>
        <content tag="javascript">
            <script src="https://unpkg.com/@egoist/snackbar/dist/snackbar.min.js"></script>
            <script>
                function messageStatus(message, type){
                    var messageStatus = " <div class=\"alert alert-dark-"+type+" alert-dismissible fade show\" id=\"messageVal\">\n" +
                        "                     <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>\n" + message +
                        "                 </div>";
                    return messageStatus;
                }

                function spanBadgeStatus(status, type){
                    var span = "<span class=\"badge badge-"+type+"\">"+status+"</span>";
                    return span
                }

                $(".enable-check").on("click", function(){
                    var checked = $(this).prop('checked');
                    var name = $(this).prop('name');
                    var id = name.split('-')
                    var idUser = id[1]
                    var label = "label[name='label-"+idUser+"']"
                    var spanStatus = ""
                    if(checked){
                        console.log("checked true = "+checked)
                        $.ajax({
                            method: "GET",
                            url: "/projet/user/updateStatusUser",
                            data: { iduser: idUser, status: checked },
                            success: function (res) {
                                $(label).html('');
                                spanStatus = spanBadgeStatus("Actif", "success")
                                $(label).append(spanStatus)
                                snackbar.createSnackbar(res.message,{
                                    timeout: 3000
                                })
                            }
                        })
                    }
                    else{
                        console.log("checked false = "+checked)
                        $.ajax({
                            method: "GET",
                            url: "/projet/user/updateStatusUser",
                            data: { iduser: idUser, status: checked },
                            success: function (res) {
                                $(label).html('')
                                spanStatus = spanBadgeStatus("Inactif", "danger")
                                $(label).append(spanStatus)
                                snackbar.createSnackbar(res.message,{
                                    timeout: 3000
                                })
                            }
                        })
                    }
                })
            </script>
        </content>
    </footer>
</html>