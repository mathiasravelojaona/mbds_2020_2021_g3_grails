package Forms.Classes

import java.sql.Timestamp
import java.text.SimpleDateFormat

class SearchDaysFilter {
    private Date d1
    private Date d2


    void setD1(String d1) {
        try{
            this.d1 = new SimpleDateFormat('dd/MM/yyyy').parse(d1)
            Calendar c1 = Calendar.getInstance()
            c1.setTimeInMillis(this.d1.getTime())
            c1.set(Calendar.HOUR_OF_DAY, 0)
            c1.set(Calendar.MINUTE, 0)
            c1.set(Calendar.SECOND, 0)
            c1.set(Calendar.MILLISECOND, 0)
            this.d1.setTime(c1.getTimeInMillis())
        }
        catch (Exception e){
            this.d1 = null
        }
    }
    void setD2(String d2) {
        try{
            this.d2 = new SimpleDateFormat('dd/MM/yyyy').parse(d2)
            Calendar c1 = Calendar.getInstance()
            c1.setTimeInMillis(this.d2.getTime())
            c1.set(Calendar.HOUR_OF_DAY, 23)
            c1.set(Calendar.MINUTE, 59)
            c1.set(Calendar.SECOND, 59)
            c1.set(Calendar.MILLISECOND, 0)
            this.d2.setTime(c1.getTimeInMillis())
        }
        catch (Exception e){
            this.d2 = null
        }
    }
    Date getD1(){
        return this.d1
    }

    Date getD2(){
        return this.d2
    }
    SearchDaysFilter(String d1,String d2){
        this.setD1(d1)
        this.setD2(d2)
    }
}
