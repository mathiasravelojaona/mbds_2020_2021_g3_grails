package com.mbds.grails

import Forms.Classes.CreateUserForm
import grails.gorm.transactions.Transactional
import grails.validation.ValidationException
import grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.web.multipart.MultipartFile
import util.AppUtil
@Transactional
class UserRoleService {

    UserService userService
    ObjectMapperService objectMapperService

    User createUser(CreateUserForm createUserForm){
        if(!createUserForm.validate()){
            throw new ValidationException('Une erreur de validation est survenue',createUserForm.errors)
        }
        if(User.findByUsername(createUserForm.username)!=null){
            throw new Exception("Cet utilisateur existe déjà")
        }
        User user = objectMapperService.map(createUserForm)
        user.save(flush: true)
        Role role = Role.findById(Long.valueOf(createUserForm.role))
        UserRole.create user, role, true
        return user
    }

    def findByAuthority(String authority){
        Role.findByAuthority(authority)
    }

    def uploadCSV(def request){
        String delimiter = ";"
        MultipartFile filecsv = request.getFile( 'filecsv' )
        BufferedReader br = new BufferedReader(new InputStreamReader(filecsv.inputStream, "UTF-8"))
        String line = ""
        String[] tempArr
        while((line = br.readLine()) != null) {
            tempArr = line.split(delimiter)
//            println(tempArr + " ")
            def nom= tempArr[0]
            def password = tempArr[1]
            def role = tempArr[2]
            println(nom)
            println(password)
            println(role)
            def newuser = new User(username: nom, password: password).save()
            def newrole = findByAuthority(role)
            UserRole.create(newuser, newrole, true)
        }
    }

    def update(User user, Role role){
        UserRole.removeAll(user)
        UserRole.create user, role, true
    }

    Role getUserRole(User user){
        def userRole = UserRole.findByUser(user)
        if(!userRole)
            return null
        return userRole.role
    }

    def listRoles(){
        return Role.list()
    }

    def deleteUser(User user){
        UserRole.removeAll(user)
        userService.delete(user.id)
    }
}
