package Interceptors


class SetCurentUserInterceptor {

    def springSecurityService
    public SetCurentUserInterceptor(){
        match controllerName: '*'
    }

    boolean before() {
        if (springSecurityService.isLoggedIn()){
            request.setAttribute("current_user", springSecurityService.currentUser)
        }
        return true
    }
}
