<%--
  Created by IntelliJ IDEA.
  User: Andi
  Date: 12/03/2021
  Time: 16:06
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<g:set var="current_user" value="${request.getAttribute("current_user")}" />
<html>
<head>
  <meta name="layout" content="main" />
</head>

<body>
<div class="card mb-4">
  <h6 class="card-header">Modification Utilisateur</h6>
  <div class="card-body">
    <g:if test="${flash.message}">
      <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:form controller="user" action="update" id="${user.id}" method="PUT">
      <div class="form-group">
        <label class="form-label">Nom d'utilisateur</label>
        <input type="text" class="form-control" placeholder="Nom" value="${user.username}" name="username">
        <g:renderErrors bean="${user}" as="list" field="username"/>
      </div>
      <g:if test="${user.id != current_user.id}">
        <div class="form-group">
        <label class="custom-control custom-checkbox">
          <input type="checkbox" value="true" class="custom-control-input" ${user.enabled ? "checked" : "" } name="enabled">
          <span class="custom-control-label">Activer</span>
          <g:renderErrors bean="${user}" as="list" field="enabled"/>
        </label>
      </div>
      <div class="form-group">
        <label class="custom-control custom-checkbox">
          <input type="checkbox" value="true" class="custom-control-input"  ${user.accountExpired ? "checked" : "" } name="accountExpired">
          <span class="custom-control-label">Compte expiré</span>
          <g:renderErrors bean="${user}" as="list" field="accountExpired"/>
        </label>
      </div>
        <div class="form-group">
          <label class="custom-control custom-checkbox">
            <input type="checkbox" value="true" class="custom-control-input"  ${user.accountLocked ? "checked" : "" } name="accountLocked">
            <span class="custom-control-label">Compte vérrouillé</span>
            <g:renderErrors bean="${user}" as="list" field="accountLocked"/>
          </label>
        </div>
        <div class="form-group">
          <label class="custom-control custom-checkbox">
            <input type="checkbox" value="true" class="custom-control-input"  ${user.passwordExpired ? "checked" : "" } name="passwordExpired">
            <span class="custom-control-label">Mot de passe expiré </span>
            <g:renderErrors bean="${user}" as="list" field="passwordExpired"/>
          </label>
        </div>
      </g:if>
      <div class="form-group">
        <label class="form-label">Role</label>
        <select class="custom-select" name="role">
          <option value=""></option>
          <g:each in="${roleList}" var="role" status="i">
            <g:if test="${userRole.id == role.id}">
              <option value="${role.id}" selected>${role.authority}</option>
            </g:if>
            <g:else>
              <option value="${role.id}">${role.authority}</option>
            </g:else>
          </g:each>
        </select>
      </div>
      <button type="submit" class="btn btn-primary">Modifier</button>
      <g:link controller="user" action="show" id="${user.id}">
        <button type="button" class="btn btn-primary">Annuler</button>
      </g:link>
    </g:form>
  </div>
</div>

</body>
</html>