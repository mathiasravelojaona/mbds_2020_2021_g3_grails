<g:set var="current_user" value="${request.getAttribute("current_user")}" />
<nav class="layout-navbar navbar navbar-expand-lg align-items-lg-center bg-dark container-p-x" id="layout-navbar">

    <!-- Brand demo (see assets/css/demo/demo.css) -->
    <a href="index.html" class="navbar-brand app-brand demo d-lg-none py-0 mr-4">
        <span class="app-brand-text demo font-weight-normal ml-2">Empire</span>
    </a>

    <!-- Sidenav toggle (see assets/css/demo/demo.css) -->
    <div class="layout-sidenav-toggle navbar-nav d-lg-none align-items-lg-center mr-auto">
        <a class="nav-item nav-link px-0 mr-lg-4" href="javascript:">
            <i class="ion ion-md-menu text-large align-middle"></i>
        </a>
    </div>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#layout-navbar-collapse">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse collapse" id="layout-navbar-collapse">
        <!-- Divider -->
        <hr class="d-lg-none w-100 my-2">
        <g:form controller="annonce" action="index" name="searchForm" method="get" class="tm-contact-form">
            <div class="navbar-nav align-items-lg-center">
                <!-- Search -->
                <label class="nav-item navbar-text navbar-search-box p-0 active">
                    <button style="background-color: white;" type="submit" class="btn btn-prinary" name="submit">
                        <i class="feather icon-search navbar-icon align-middle"></i>
                    </button>
                    <span class="navbar-search-input pl-2">
                        <input type="text" id="search"
                               class="form-control navbar-text mx-2 searchInput"
                               value="${keyword_annonce ? keyword_annonce : ''}" name="search" placeholder="rechercher...">
                    </span>
                </label>
            </div>
        </g:form>

        <div class="navbar-nav align-items-lg-center ml-auto">

            <!-- Divider -->
            <div class="nav-item d-none d-lg-block text-big font-weight-light line-height-1 opacity-25 mr-3 ml-1">|</div>
            <div class="demo-navbar-user nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                    <span class="d-inline-flex flex-lg-row-reverse align-items-center align-middle">
                        <span class="px-1 mr-lg-2 ml-2 ml-lg-0">${current_user.username}</span>
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <div style="width:100%;padding:15px;display: flex;justify-content: center;align-items: center;">
                        <div class="round-user">
                            <i class="fas fa-user"></i>
                        </div>
                    </div>
                    <div class="roles" style="width: 100%">
                        <sec:ifAllGranted roles='ROLE_ADMIN'>
                            <p class="connected_name"
                               style="text-align: center;width:100%;">Administrateur</p>
                        </sec:ifAllGranted>
                        <sec:ifAllGranted roles='ROLE_MODO'>
                            <p class="connected_name"
                               style="text-align: center;width:100%;">Modérateur</p>
                        </sec:ifAllGranted>
                    </div>
                    <div class="dropdown-divider"></div>
                    <g:link controller="user" action="logout" class="link">
                        <i class="feather icon-power text-danger"></i> Déconnexion
                    </g:link>
                    <g:link controller="user" action="configuration" class="link">
                        <i class="fas fa-cog text-danger"></i> Configuration
                    </g:link>
            </div>
        </div>
    </div>
</nav>
