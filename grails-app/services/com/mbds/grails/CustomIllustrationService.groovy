package com.mbds.grails

import Forms.Classes.GallerySearchFilter
import grails.gorm.transactions.Transactional
import grails.orm.PagedResultList
import grails.validation.ValidationException
import org.springframework.web.multipart.MultipartFile

@Transactional
class CustomIllustrationService {
    IllustrationService illustrationService
    AnnonceService annonceService
    CDNService CDNService

    def addIllustration(Long idAnnonce,MultipartFile multipartFile) {
        if(multipartFile.empty){
            throw new Exception("le fichier que vous tentez d'utiliser est vide")
        }
        Annonce annonce = annonceService.get(idAnnonce)
        if(!annonce){
            throw new Exception("Cette annonce n'existe pas")
        }
        Illustration illustration = new Illustration()
        Map resultat = this.CDNService.doUpload(multipartFile)
        illustration.filename = resultat.get('url').toString()
        annonce.addToIllustrations(illustration)
        annonce.save()
        return illustration
    }

    def listAnnonceIllustrations(Annonce annonce, int max, int offset){
        return Illustration.findAllByAnnonce(annonce,[max:max,offset:offset]);
    }
    Closure searchIllustrationClosure(GallerySearchFilter gallerySearchFilter){
        Closure closure_keyword = { builder ->
            if(gallerySearchFilter.keyword != '' || gallerySearchFilter.keyword==null){
                    builder.annonce{
                        builder.or{
                            ilike('description','%'+gallerySearchFilter.getKeyword()+'%')
                            ilike('title','%'+gallerySearchFilter.getKeyword()+'%')
                        }
                    }
            }
        }
        Closure closure_authors_id = { builder ->
            def data = gallerySearchFilter.authors
            if(data){
                builder.annonce{
                    author{
                        builder.'in'('id',data)
                    }
                }
            }
        }
        Closure closure_date = { builder ->
            if(gallerySearchFilter.d1 && gallerySearchFilter.d2){
                if(gallerySearchFilter.galleryDate){
                    builder.between('dateCreated',gallerySearchFilter.d1,gallerySearchFilter.d2)
                }
                else{
                    builder.annonce{
                        between('dateCreated',gallerySearchFilter.d1,gallerySearchFilter.d2)
                    }
                }
            }
            else {
                if(gallerySearchFilter.galleryDate){
                    builder.and{
                        if(gallerySearchFilter.d1){
                            builder.ge('dateCreated',gallerySearchFilter.d1)
                        }
                        if(gallerySearchFilter.d2){
                            builder.le('dateCreated',gallerySearchFilter.d2)
                        }
                    }
                }
                else{
                    builder.annonce{
                        and{
                            if(gallerySearchFilter.d1){
                                builder.ge('dateCreated',gallerySearchFilter.d1)
                            }
                            if(gallerySearchFilter.d2){
                                builder.le('dateCreated',gallerySearchFilter.d2)
                            }
                        }
                    }
                }
            }
        }
        Closure closure = { builder ->
            builder.and{
                closure_authors_id(builder)
                closure_date(builder)
                closure_keyword(builder)
            }
        }
        return closure
    }

    PagedResultList search(GallerySearchFilter gallerySearchFilter, int max = 10){
        def page = gallerySearchFilter.page - 1
        def criteria = Illustration.createCriteria()
        Closure closurefinale = this.searchIllustrationClosure(gallerySearchFilter)
        PagedResultList resultList = criteria.list(max:max,offset:page*max){
            closurefinale(criteria)
            if(gallerySearchFilter.galleryDate){
                order('dateCreated',gallerySearchFilter.order.toLowerCase())
            }
            else{
                annonce {
                    order('dateCreated',gallerySearchFilter.order.toLowerCase())
                }
            }
        }
        return resultList
    }
}
