package Forms.Abstractions

import grails.validation.Validateable

abstract class BasicFormUpdateModel implements Validateable{
    public String id

    static constraints = {
        id nullable: false,blank: false, validator: { value,obj,errors ->
            try{
                def newval = Long.valueOf(value)
                return true
            }
            catch (Exception e){
                errors.rejectValue('id','blank')
                return false
            }
        }
    }
}
