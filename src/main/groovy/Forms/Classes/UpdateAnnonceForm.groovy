package Forms.Classes

import Forms.Abstractions.BasicFormUpdateModel
import com.mbds.grails.User
import grails.validation.Validateable


class UpdateAnnonceForm extends BasicFormUpdateModel{

    String price
    String title
    String description
    String idAuthor


    static constraints = {
        price nullable: false, blank: false, validator: {value ->
            try{
                def newval = Double.valueOf(value)
                return true
            }
            catch (Exception e){
                errors.rejectValue('price','blank')
                return false
            }
        }
        title nullable: false, blank: false
        description nullable: false, blank: false
        idAuthor nullable: false,blank: false, validator: {value ->
            try{
                def newval = Long.valueOf(value)
                User auteur = User.get(newval)
                if(!auteur){
                    throw new Exception()
                }
                return true
            }
            catch (Exception e){
                errors.rejectValue('idAuthor','blank')
                return false
            }
        }
    }
}
