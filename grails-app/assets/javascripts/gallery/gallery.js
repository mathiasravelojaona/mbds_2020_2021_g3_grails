const searchAuthors = function(keyword){
    return new Promise((resolve,reject) => {
        $.ajax({
            url: `/projet/gallery/searchAuthors?keyword=${keyword}`,
            type: "get",
            contentType: 'application/json',
            dataType: 'json',
            success: function(data) {
                resolve(data);
            },
            error: function(xhr){
                reject(xhr);
            }
        });
    });
};
const deleteIllustration = function(idIllustration){
    return new Promise((resolve,reject) => {
        $.ajax({
            url: `/projet/illustration/delete/${idIllustration}`,
            type: "delete",
            contentType: 'application/json',
            dataType: 'json',
            success: function(data) {
                resolve(data);
            },
            error: function(xhr){
                reject(xhr);
            }
        });
    });
};

const searchGallery = function(keyword,authors,range,order,galleryDate,page){
    return new Promise((resolve,reject) => {
        $.ajax({
            url: `/projet/gallery/search`,
            type: "POST",
            data: JSON.stringify({
                keyword: keyword,
                authors: authors.map(value => value.key).join(','),
                d1:range.start ? moment(range.start).format('DD/MM/YYYY') : null,
                d2:range.end ? moment(range.end).format('DD/MM/YYYY') : null ,
                order: order ? "asc": "desc",
                galleryDate,
                page,
            }),
            contentType: 'application/json',
            dataType: 'json',
            success: function(data) {
                resolve(data);
            },
            error: function(xhr){
                reject(xhr);
            }
        });
    });
};
Vue.use(VueLoading);
Vue.use(Toasted);
Vue.component('gallery', VueLoading);
let application = new Vue({
    el: document.getElementById('gallery'),
    delimiters: ['[%', '%]'],
    data: function(){
        return {
            selectedTags:[],
            range: {
                start: null,
                end: null
            },
            galleryDate: false,
            keyword: '',
            searchResults:[],
            asc: false,
            page: 1,
            liste: [],
            pagenumbers: 0,
        }
    },
    components: { "tags-input": VoerroTagsInput },
    methods:{
        changeOrder(){
          this.asc = !this.asc;
          this.page = 1;
          this.refreshList();
        },
        getLoader(){
            return Vue.$loading.show({
                container: this.$refs.loadingContainer,
                canCancel: true, // default false
                onCancel: this.yourCallbackMethod,
                color: '#000000',
                loader: 'spinner',
                width: 64,
                height: 64,
                backgroundColor: '#ffffff',
                opacity: 0.5,
                zIndex: 999,
            });
        },
        onKeyUp(value){
            this.updateMatchingAuthors(value);
        },
        onTagsUpdated(){

        },
        setGalleryDate(value){
            this.page = 1;
            this.galleryDate = value;
            this.refreshList();
        },
        mapAuthors(data){
          return data.map(value => ({
              key:value.id,
              value:value.name,
          }));
        },
        async updateMatchingAuthors(text){
            try{
                let authors = await searchAuthors(text);
                Vue.set(this,'searchResults',this.mapAuthors(authors));
            }
            catch (e) {
                console.log(e);
                let myToast = this.$toasted.show("Une erreur de connexion est survenue");
                myToast.goAway(1500);
            }
        },
        async removeIllustration(id){
          const loader = this.getLoader();
          try{
              console.log("ato");
              const resultat = await deleteIllustration(id);
              console.log(resultat);
              let myToast = this.$toasted.show(resultat.message);
              myToast.goAway(1500);
              this.refreshList();
          }
          catch (e) {
              console.log(e);
              let myToast = this.$toasted.show("Une erreur de connexion est survenue");
              myToast.goAway(1500);
          }
          finally {
              loader.hide();
          }
        },
        async refreshList(){
          const loader = this.getLoader();
          try{
              const data = await searchGallery(this.keyword,
                  this.selectedTags,
                  this.range,
                  this.asc,
                  this.galleryDate,
                  this.page);
              console.log(data);
              Vue.set(this,'liste',data.data);
              this.pagenumbers = data.pages;
          }
          catch (e) {
              console.log(e);
              let myToast = this.$toasted.show("Une erreur de connexion est survenue");
              myToast.goAway(1500);
          }
          finally {
              loader.hide();
          }
        },
        filtrer_par_auteur(){
            this.page = 1;
            this.refreshList();
        },
        goTo(pagenumber){
            this.page = pagenumber;
            this.refreshList();
        },
        deleteDateFilter(){
          this.page = 1;
          Vue.set(this,'range',{
              start:null,
              end: null,
          });
          this.refreshList();
        },
    },
    computed:{
        hasDate(){
            return this.range.start;
        },
    },
    mounted(){
        this.refreshList();
    }
});