package com.mbds.grails

import Forms.Classes.SearchDaysFilter
import Forms.Classes.SearchDowFilter
import Forms.Classes.SearchMonthFilter
import Forms.Classes.TopAuthorsFilter
import grails.gorm.transactions.Transactional

@Transactional
class DashboardService {

    Map statistiques() {
        return [
                nombre_articles: nombreArticles(),
                nombre_utilisateurs: nombreUtilisateurs(),
                nombre_illustrations: nombreIllustrations(),
                nombre_annonce_par_utilisateur: nombreAnnonceParUtilisateur(),
                nombre_articles_par_jour: nombreArticlesParJour(),
                nombre_articles_par_mois: nombreArticlesParMois(),
                nombre_articles_par_jour_semaine: nombreArticlesParJourDeLaSemaine(),
                top_auteurs: topUtilisateurs(),
        ]
    }
    def nombreArticles(){
        def criteria_new = Annonce.createCriteria()
        def resultat = criteria_new.list{
            projections {
                count('id')
            }
        }
        return resultat[0]
    }
    def nombreUtilisateurs(){
        def criteria_new = User.createCriteria()
        def resultat = criteria_new.list{
            projections {
                count('id')
            }
        }
        return resultat[0]
    }
    def nombreIllustrations(){
        def criteria_new = Illustration.createCriteria()
        def resultat = criteria_new.list{
            projections {
                count('id')
            }
        }
        return resultat[0]
    }
    Map nombreUserParRole(){
        def criteria_new = UserRole.createCriteria()
        def resultat = criteria_new.list{
            projections {
                groupProperty('role')
                count('id')
            }
        }
        return resultat
    }
    def nombreAnnonceParUtilisateur(int top = 3){
        def criteria_new = Annonce.createCriteria()
        def resultat = criteria_new.list(max: top){
            projections {
                groupProperty('author')
                count('id')
            }
        }
        return resultat
    }
    def nombreArticlesParJour(SearchDaysFilter searchDaysFilter){
        def criteria_new = Annonce.createCriteria()
        def resultat = criteria_new.list{
            if(searchDaysFilter){
                if(searchDaysFilter.d1){
                    ge('dateCreated',searchDaysFilter.d1)
                }
                if(searchDaysFilter.d2){
                    le('dateCreated',searchDaysFilter.d2)
                }
            }
            projections {
                groupProperty('createdDayFormatted')
                count('id')
            }
        }
        return resultat
    }
    def nombreArticlesParMois(SearchMonthFilter searchMonthFilter){
        def criteria_new = Annonce.createCriteria()
        def resultat = criteria_new.list{
            if(searchMonthFilter && searchMonthFilter.getYear()){
                eq("year",searchMonthFilter.year)
            }
            else{
                eq("year",UtilService.getCurrentYear())
            }
            projections {
                groupProperty('month')
                count('id')
            }
            order("month","asc")
        }
        return resultat
    }
    def nombreArticlesParJourDeLaSemaine(SearchDowFilter searchDowFilter){
        def criteria_new = Annonce.createCriteria()
        def resultat = criteria_new.list{
            if(searchDowFilter){
                if(searchDowFilter.year){
                    eq("year",searchDowFilter.year)
                }
                if(searchDowFilter.month){
                    eq("month",searchDowFilter.month)
                }
            }
            projections {
                groupProperty('dayOfWeek')
                count('id')
            }
            order("dayOfWeek","asc")
        }
        return resultat
    }
    def topUtilisateurs(TopAuthorsFilter topAuthorsFilter){
        def criteria_new = Annonce.createCriteria()
        int top = topAuthorsFilter ? topAuthorsFilter.top : 5
        def top_data = criteria_new.list(max: top){
            if(topAuthorsFilter){
                if(topAuthorsFilter.month){
                    eq("month",topAuthorsFilter.month)
                }
            }
            projections {
                groupProperty('author')
                count('id','value')
            }
            order("value",'desc')
        }
        return top_data
    }
}
