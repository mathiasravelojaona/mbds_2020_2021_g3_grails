<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <asset:stylesheet src="annonces.css" />
    </head>
    <body>
        <div id="list-annonce" class="content scaffold-list" role="main">
            <h1> Liste des annonces </h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <div class="export_button" style="display: flex; justify-content: flex-end";>
                <g:link controller="annonce" action="export" class="btn btn-lg btn-outline-primary">Export</g:link>
            </div>
            <table class="table table-bordered table">
                <thead>
                <tr>
                        <g:sortableColumn class="field_text" property="title"
                                      title="Titre" />
                        <g:sortableColumn class="field_text" property="description"
                                          title="Description" />
                        <g:sortableColumn class="field_text" property="dateCreated"
                                          title="Date de création" />
                        <g:sortableColumn class="field_number" property="price"
                                          title="Prix (${'$'})" />
                        <g:sortableColumn class="field_text" property="suthor"
                                          title="Auteur" />
                </tr>
                </thead>
                <tbody>
                <g:each in="${annonceList}" var="bean" status="i">
                        <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                            <td class="field_text">
                                <g:link method="GET" resource="${bean}">
                                    <p class="field_text">${bean.title}</p>
                                </g:link>
                            </td>
                            <td class="field_text">
                                ${raw(bean.description)}
                            </td>
                            <td class="field_text">
                                <g:formatDate format="dd-MM-yyyy" date="${bean.dateCreated}"/>
                            </td>
                            <td class="field_number">
                                <g:formatNumber number="${bean.price}" />
                            </td>
                            <td class="field_text">
                                <p class="field_text">${bean.author.username}</p>
                            </td>
                        </tr>
                </g:each>
                </tbody>
            </table>
            <div annonce class="pagination">
                <g:paginate total="${annonceCount ?: 0}" next="Suivant" prev="Précédant" params="${['search':keyword_annonce ? keyword_annonce : '']}" />
            </div>
        </div>
    </body>
</html>