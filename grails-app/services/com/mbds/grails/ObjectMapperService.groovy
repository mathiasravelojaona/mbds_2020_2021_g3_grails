package com.mbds.grails

import Forms.Classes.CreateUserForm
import Forms.Classes.InsertAnnonceForm
import Forms.Classes.UpdateAnnonceForm
import grails.gorm.transactions.Transactional

@Transactional
class ObjectMapperService {

    UpdateAnnonceForm map(Annonce annonce) {
        UpdateAnnonceForm model = new UpdateAnnonceForm(id: annonce.id.toString(),
                idAuthor: annonce.author.id.toString(),
                description: annonce.description,
                title: annonce.title,
                price: annonce.price)
        return model
    }
    Annonce map(UpdateAnnonceForm updateFormModel) {
        Long id = Long.parseLong(updateFormModel.id)
        Annonce annonce = Annonce.get(id)
        annonce.price = Double.valueOf(updateFormModel.price)
        annonce.title = updateFormModel.title
        annonce.description = updateFormModel.description
        annonce.author = User.get(Long.parseLong(updateFormModel.idAuthor))
        return annonce
    }
    Annonce map(InsertAnnonceForm insertAnnonceForm){
        Annonce annonce = new Annonce()
        annonce.price = Double.valueOf(insertAnnonceForm.price)
        annonce.title = insertAnnonceForm.title
        annonce.description = insertAnnonceForm.description
        annonce.author = User.get(Long.parseLong(insertAnnonceForm.idAuthor))
        return annonce
    }
    User map(CreateUserForm createUserForm){
        User user = new User()
        user.username = createUserForm.username
        user.password = createUserForm.password
        return user
    }
}
