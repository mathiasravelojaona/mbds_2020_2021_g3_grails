package com.mbds.grails

import Forms.Classes.GallerySearchFilter
import grails.converters.JSON
import grails.orm.PagedResultList
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN', 'ROLE_MODO'])
class GalleryController {
    CustomUserService customUserService
    CustomIllustrationService customIllustrationService

    def index() {
        return
    }
    def searchAuthors(){
        render customUserService.getAuthors(params.keyword) as JSON
    }

    def search(){
        def jsonObject = request.JSON
        GallerySearchFilter gallerySearchFilter = new GallerySearchFilter(jsonObject.keyword,
                jsonObject.authors,
                jsonObject.d1 ? jsonObject.d1 : "",
                jsonObject.d2 ? jsonObject.d2 : "",
                jsonObject.order,
                jsonObject.galleryDate ? "true": "false",
                jsonObject.page ? "" + jsonObject.page : 1)

        PagedResultList resultat = customIllustrationService.search(gallerySearchFilter,10)
        def map = [pages: UtilService.getPageNumbers(resultat.totalCount.intValue(),10),
                   currentpage: gallerySearchFilter.page,
                   data: resultat,
                   params:gallerySearchFilter]
        render map as JSON
    }
}
