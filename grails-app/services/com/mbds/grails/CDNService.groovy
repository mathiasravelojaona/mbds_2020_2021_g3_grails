package com.mbds.grails

import com.cloudinary.Cloudinary
import grails.config.Config
import grails.core.support.GrailsConfigurationAware
import grails.gorm.transactions.Transactional
import groovy.transform.CompileStatic
import org.springframework.web.multipart.MultipartFile

import java.text.SimpleDateFormat

class CDNService implements GrailsConfigurationAware{

    def directory
    Cloudinary cloudinary
    UtilService utilService

    @Override
    void setConfiguration(Config co) {
        Map config = new HashMap()
        config.put("cloud_name", co.getRequiredProperty("cloudinary.cloud_name"))
        config.put("api_key",co.getRequiredProperty("cloudinary.api_key"))
        config.put("api_secret",co.getRequiredProperty("cloudinary.api_secret"))
        this.cloudinary = new Cloudinary(config)
        this.directory = co.getRequiredProperty("cloudinary.dir_name")
    }
    Map doUpload(MultipartFile multipartFile){
        def date_now = new SimpleDateFormat("dd_MM_yyyy_HH_mm").format(new Date())
        Map paramsUpload = [public_id: this.directory + "/" + date_now +"_"+  utilService.encodeString(multipartFile.getOriginalFilename()),
                            overwrite: true]
        def response = cloudinary.uploader().upload(multipartFile.bytes, paramsUpload)
        response
    }
}
