<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <asset:stylesheet rel="stylesheet" href="annonces/annonces.css" />
        <style>
        .form-group{
            padding: 15px;
        }
        </style>
    </head>
    <body>
        <div id="create-annonce" class="content scaffold-create" role="main">
            <div class="card">
                <div class="card-header">
                    <h2 style="margin-bottom: 0;">Création d'une annonce</h2>
                </div>
                <div class="card-body body">
                    <g:if test="${flash.message}">
                        <div class="message" role="status">${flash.message}</div>
                    </g:if>
                    <g:uploadForm controller="annonce" action="save" method="POST">
                        <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="titre">Titre <span class="required-indicator">*</span></label>
                                <input type="text" class="form-control" id="title" name="title" value="${annonce.title}" placeholder="Titre annonce">
                                <g:renderErrors bean="${annonce}" as="list" field="title"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="price">Prix($) <span class="required-indicator">*</span></label>
                                <input type="number decimal" name="price" class="form-control" value="${annonce.price}" id="price"/>
                                <g:renderErrors bean="${annonce}" as="list" field="price"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="author">Auteur <span class="required-indicator">*</span></label>
                                <select class="form-control" id="author" name="Authorid">
                                    <g:each in="${userList}" var="user">
                                            <option value="${user.id}">${user.username}</option>
                                    </g:each>
                                </select>
                            </div>
                        </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea id="description" name="description" class="form-control">
                                        ${raw(annonce.description)}
                                    </textarea>
                                    <g:renderErrors bean="${annonce}" as="list" field="description"/>
                                </div>
                            </div>
                        <div class="form-group col-md-6">
                            <label class="form-label">Illustration</label>
                            <input type="file" class="form-control" id="illustration" name="illustration" value="${illustration}">
                            <div class="clearfix"></div>
                        </div>
                        <button type="submit" style="width:100%;" class="btn btn-primary">Sauvegarder</button>
                        </div>
                    </g:uploadForm>
                </div>
            </div>
        </div>
    </body>
<footer>
    <content tag="javascript">
        <script>
            tinymce.init({
                selector: '#description'
            });
        </script>
    </content>
</footer>
</html>
