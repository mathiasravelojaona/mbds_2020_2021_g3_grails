
<!DOCTYPE html>

<html lang="en" class="material-style layout-fixed">

<head>
    <title>Lecoincoin - Administration </title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="Empire Bootstrap admin template made using Bootstrap 4, it has tons of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
    <meta name="keywords" content="Empire, bootstrap admin template, bootstrap admin panel, bootstrap 4 admin template, admin template">
    <meta name="author" content="Srthemesvilla" />
    <link rel="icon" type="image/x-icon" href="assets/images/template-1/favicon.ico">
    <!-- Google fonts -->

    <!-- Icon fonts -->
    <asset:stylesheet rel="stylesheet" href="template-1/fonts/fontawesome.css" />
    <asset:stylesheet rel="stylesheet" href="application.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/fonts/ionicons.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/fonts/linearicons.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/fonts/open-iconic.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/fonts/pe-icon-7-stroke.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/fonts/feather.css" />

    <!-- Core stylesheets -->
    <asset:stylesheet rel="stylesheet" href="template-1/bootstrap-material.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/shreerang-material.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/uikit.css" />

    <!-- Libs -->
    <asset:stylesheet rel="stylesheet" href="template-1/pages/authentication.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/libs/perfect-scrollbar/perfect-scrollbar.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/libs/morris/morris.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/libs/flot/flot.css" />
    <asset:stylesheet href="vue_loading_overlay.css" />
    <style type="text/css">
        .pagination span, .pagination a {
            display: inline-block;
            height: 31px;
            text-decoration: none;
            background-color: whitesmoke;
            transition: background-color .3s;
            border: 1px solid #ddd;
            padding: 5px;
            text-align: center;
            border-radius: 2px;
            margin: 0 1px;
            color: rgba(0, 0, 0, 0.54);
            font-weight: bold;
        }

        .step.gap {
            display: none;
            /* ^ if we don't want to see the dots*/
        }

        .nextLink, .prevLink {

        }
        [v-cloak] {display: none !important;}

        .pagination .currentStep {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }

        a:hover:not(.active) {
            background-color: #ddd;
        }
    </style>
    <g:layoutHead/>
</head>

<body>
<!-- [ Preloader ] Start -->
<div class="page-loader">
    <div class="bg-primary"></div>
</div>
<div class="layout-wrapper layout-2">
    <div class="layout-inner">
        <g:render template="/shared/sidenav" />
        <div class="layout-container">
            <g:render template="/shared/navbar" />
            <div class="layout-content">
                <div class="container-fluid flex-grow-1 container-p-y">
                    <g:layoutBody/>
                </div>
            </div>
        </div>
    </div>
    <div class="layout-overlay layout-sidenav-toggle"></div>
</div>

</body>
<footer>
    <!-- [ Layout wrapper] End -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
%{--    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>--}%
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <asset:javascript src="moment.js" />
    <!-- Core scripts -->
    <asset:javascript src="template-1/libs/popper/popper.js"/>
    <asset:javascript src="template-1/bootstrap.js"/>
    <asset:javascript src="template-1/sidenav.js"/>
    <asset:javascript src="template-1/layout-helpers.js"/>
    <asset:javascript src="template-1/material-ripple.js"/>
    <asset:javascript src="vue.js" />
    <asset:javascript src="vue-toasted.js" />
    <asset:javascript src="vue_loading_overlay.js" />
    <asset:javascript src="v_calendar.js" />
    <!-- Libs -->
    <asset:javascript src="template-1/libs/perfect-scrollbar/perfect-scrollbar.js"/>

    <!-- Demo -->
    <asset:javascript src="template-1/demo.js"/>
    <script src="https://cdn.tiny.cloud/1/9cr52bvov3w85onu0ztew2yksy138zzzkkp0jl3cg5ejjug8/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="https://cdn.tiny.cloud/1/9cr52bvov3w85onu0ztew2yksy138zzzkkp0jl3cg5ejjug8/tinymce/5/jquery.tinymce.min.js" referrerpolicy="origin"></script>

    <g:pageProperty name="page.javascript"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
</footer>
</html>
