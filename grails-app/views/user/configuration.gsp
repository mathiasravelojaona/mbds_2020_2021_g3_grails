<%--
  Created by IntelliJ IDEA.
  User: Andi
  Date: 12/03/2021
  Time: 16:06
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<g:set var="current_user" value="${request.getAttribute("current_user")}" />
<html>
<head>
    <meta name="layout" content="main" />
</head>

<body>
<div class="card mb-4">
    <h6 class="card-header">Configurer votre compte</h6>
    <div class="card-body">
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
        <g:form controller="user" action="configurerUsername" method="POST" style="padding-bottom: 15px;">
            <div class="form-group">
                <label class="form-label">Votre nom</label>
                <input type="text" class="form-control" placeholder="Nom" value="${user.username}" name="username">
                <g:renderErrors bean="${user}" as="list" field="username"/>
            </div>
            <button type="submit" class="btn btn-primary">Modifier</button>
        </g:form>
        <g:form controller="user" action="configurerPassword" method="POST" style="padding-bottom: 15px;">
            <div class="form-group">
                <label class="form-label">Votre mot de passe</label>
                <input type="text" class="form-control" placeholder="Mot de passe" value="" name="password">
                <g:renderErrors bean="${user}" as="list" field="password"/>
            </div>
            <button type="submit" class="btn btn-primary">Modifier</button>
        </g:form>
    </div>
</div>

</body>
</html>