<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
    <!-- [ Layout content ] Start -->
    <div class="layout-content">

        <!-- [ content ] Start -->
        <div class="container-fluid flex-grow-1 container-p-y">
            <h4 class="font-weight-bold py-3 mb-0">Ajout d'un utilisateur</h4>
            <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Utilisateurs</li>
                    <li class="breadcrumb-item active">Ajout</li>
                </ol>
            </div>
        <div class="card mb-4">
            <h6 class="card-header">Utilisateurs</h6>
            <div class="card-body">
                <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
                </g:if>
                <g:form controller="user" action="save" method="POST">
                    <div class="form-group">
                        <label class="form-label">Nom</label>
                        <input type="text" class="form-control"
                               placeholder="Nom"
                               name="username" value="${user.username}">
                        <g:renderErrors bean="${user}" as="list" field="username"/>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Mot de passe</label>
                        <input type="password"
                               class="form-control"
                               placeholder="Mot de passe" name="password" value="${user.password}">
                        <g:renderErrors bean="${user}" as="list" field="password"/>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Role</label>
                        <select class="custom-select" name="role">
                            <option value=""></option>
                            <g:each in="${roleList}" var="role" status="i">
                                <g:if test="${user.role == role.id}">
                                    <option value="${role.id}" selected>${role.authority}</option>
                                </g:if>
                                <g:if test="${user.role != role.id}">
                                    <option value="${role.id}">${role.authority}</option>
                                </g:if>
                            </g:each>
                        </select>
                        <g:renderErrors bean="${user}" as="list" field="role"/>
                    </div>
                    <button type="submit" class="btn btn-primary">Créer</button>
                </g:form>
            </div>
        </div>
        </div>
    </body>
</html>
