<div id="layout-sidenav" class="layout-sidenav sidenav sidenav-vertical bg-white logo-dark">
    <!-- Brand demo (see assets/css/demo/demo.css) -->
    <div class="app-brand demo">
        <a href="index.html" class="app-brand-text demo sidenav-text font-weight-normal ml-2">E-Annonce</a>
    </div>
    <div class="sidenav-divider mt-0"></div>

    <!-- Links -->
    <ul class="sidenav-inner py-1">

        <!-- Dashboards -->
        <li class="sidenav-item active">

            <g:link controller="dashboard" class="sidenav-link" action="index">
                <i class="sidenav-icon  fas fa-chart-area"></i>
                <div>Tableau de bord</div>
            </g:link>
        </li>

        <!-- UI elements -->
        <li class="sidenav-item">
            <a href="javascript:" class="sidenav-link sidenav-toggle">
                <i class="sidenav-icon  fas fa-pen-square"></i>
                <div>Annonces</div>
            </a>
            <ul class="sidenav-menu">
                <li class="sidenav-item">
                    <g:link controller="annonce" class="sidenav-link" action="index">
                        <div>Liste</div>
                    </g:link>
                </li>
                <sec:ifAllGranted roles='ROLE_ADMIN'>
                    <li class="sidenav-item">
                        <g:link controller="annonce" class="sidenav-link" action="create">
                            <div>Ajout</div>
                        </g:link>
                    </li>
                </sec:ifAllGranted>
            </ul>
        </li>
        <li class="sidenav-item">
            <a href="javascript:" class="sidenav-link sidenav-toggle">
                <i class="sidenav-icon  fas fa-user"></i>
                <div>Utilisateurs</div>
            </a>
            <ul class="sidenav-menu">
                <li class="sidenav-item">
                    <g:link controller="user" class="sidenav-link" action="index">
                        <div>Liste</div>
                    </g:link>
                </li>
                <sec:ifAllGranted roles='ROLE_ADMIN'>
                    <li class="sidenav-item">
                        <g:link controller="user" class="sidenav-link" action="create">
                            <div>Ajout</div>
                        </g:link>
                    </li>
                </sec:ifAllGranted>
            </ul>
        </li>
        <li class="sidenav-item active">
            <g:link controller="gallery" class="sidenav-link" action="index">
                 <i class="sidenav-icon icon-p fas fa-image"></i>
                 <div>Gallerie</div>
            </g:link>
        </li>
    </ul>
</div>
