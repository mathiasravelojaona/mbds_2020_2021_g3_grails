package com.mbds.grails

import Forms.Classes.GallerySearchFilter
import grails.converters.JSON
import grails.orm.PagedResultList
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartRequest

import static org.springframework.http.HttpStatus.*

@Secured(['ROLE_ADMIN', 'ROLE_MODO'])
class IllustrationController {

    IllustrationService illustrationService
    CustomIllustrationService customIllustrationService


    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def show(Long id) {
        respond illustrationService.get(id)
    }
    @Secured(['ROLE_ADMIN'])
    def save() {
        response.status = 200
        def message = "L'image a bien été ajoutée"
        try{
            def id = params.id
            MultipartRequest multipartRequest =  request as MultipartRequest
            MultipartFile multipartFile = multipartRequest.getFile("importFile")
            customIllustrationService.addIllustration(Long.valueOf(id),multipartFile)
        }
        catch (Exception e){
            response.status = 500
            message = e.message
        }
        finally {
            def returnMessage = [message: message]
            render returnMessage as JSON
        }
    }


    @Secured(['ROLE_ADMIN'])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }
        response.status = 200
        String message = "L'illustration a bien été supprimée"
        try{
            illustrationService.delete(id)
        }
        catch (e){
            print e.message
            message = "Une erreur est survenue durant la suppression"
            response.status = 500
        }
        finally {
            switch (request.contentType){
                case 'application/json':
                    def returnMessage = [message: message]
                    render returnMessage as JSON
                    break
                default:
                    flash.message = message
                    redirect action:"index", method:"GET"
                    break
            }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'illustration.label', default: 'Illustration'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
