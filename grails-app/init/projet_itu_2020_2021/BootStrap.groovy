package projet_itu_2020_2021

import com.mbds.grails.Annonce
import com.mbds.grails.Illustration
import com.mbds.grails.Role
import com.mbds.grails.User
import com.mbds.grails.UserRole
import org.grails.orm.hibernate.cfg.GrailsDomainBinder

import java.text.SimpleDateFormat


class BootStrap {
    GrailsDomainBinder grailsDomainBinder


    def init = { servletContext ->
        def adminRole = new Role(authority: "ROLE_ADMIN").save()
        def modRole = new Role(authority: "ROLE_MODO").save()
        def userRole = new Role(authority: "ROLE_USER").save()

        def adminUser = new User(username: "admin", password: "password").save()
        def modUser = new User(username: "moderateur", password: "password").save()
        def userUser = new User(username: "client", password: "password").save()
        UserRole.create adminUser, adminRole, true
        UserRole.create(modUser, modRole, true)
        UserRole.create(userUser, userRole, true)
        SimpleDateFormat sdf = new SimpleDateFormat('dd/MM/yyyy')
        ArrayList<Annonce> annonces = new ArrayList<>()
        ArrayList<Date>datelist = Bootstrap_Data.getDateArray()
        //creation utilisateur pour liste utilisateur
        for(int i =0 ; i<20;i++){
            def ad = new User(username: "admin" + i, password: "password").save()
            def mod = new User(username: "moderateur" + i, password: "password").save()
            def us = new User(username: "client" + i, password: "password").save()
            UserRole.create ad, adminRole, true
            UserRole.create(mod, modRole, true)
            UserRole.create(us, userRole, true)
        }
        def taille = datelist.size()
        User.list().each {
            User userInstance ->
                def tableau = Bootstrap_Data.getInitArray()
                (1..5).each {value ->
                    def tailleTableau = 56
                    def indice = getRandomNumber(0,tailleTableau - 1)
                    def indiceDate = getRandomNumber(0,taille - 1)
                    def currentAnnonce = tableau[indice]
                    currentAnnonce.dateCreated = datelist[indiceDate]
                    (1..5).each {
                        def newIndice = getRandomNumber(0,taille - 1)
                        def illustration = new Illustration(filename: "https://res.cloudinary.com/dsh5z5f9e/image/upload/v1615995865/pictures/template_dqpraa.jpg")
                        illustration.dateCreated = datelist[newIndice]
                        currentAnnonce.addToIllustrations(illustration)
                    }
                    userInstance.addToAnnonces(currentAnnonce)
                }
                userInstance.save(flush: true, failOnError: true)
        }

        Marshaller.init()
    }
    def destroy = {}
    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }
}
