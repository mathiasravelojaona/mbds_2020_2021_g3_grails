package com.mbds.grails

import grails.gorm.PagedResultList
import grails.gorm.transactions.Transactional
import grails.validation.ValidationException
import grails.web.servlet.mvc.GrailsParameterMap


@Transactional
class CustomUserService {
    UserRoleService userRoleService
    def getAuthors(String keyword) {
        keyword = keyword ? keyword : ''
        return User.findAllByUsernameLike('%' + keyword + '%')
    }
    User updateUser(User user,boolean mine, Role role){
        User trueUser = User.findById(user.id)
        trueUser.username = user.username
        if(!mine){
            trueUser.enabled = user.enabled
            trueUser.accountExpired = user.accountExpired
            trueUser.passwordExpired = user.passwordExpired
            trueUser.accountLocked = user.accountLocked
        }
        if(!trueUser.validate()){
            throw new ValidationException('Une erreur de validation est survenue',trueUser.errors)
        }
        trueUser.save(flush: true)
        userRoleService.update(trueUser,role)
        return trueUser
    }

    User updateUserAPI(User user,Long roleId, boolean mine){
        User trueUser = User.findById(user.id)
        trueUser.username = user.username
        trueUser.password = user.password
        if(!mine){
            trueUser.enabled = user.enabled
            trueUser.accountExpired = user.accountExpired
            trueUser.passwordExpired = user.passwordExpired
            trueUser.accountLocked = user.accountLocked
        }
        if(!trueUser.validate()){
            throw new ValidationException('Une erreur de validation est survenue',trueUser.errors)
        }
        trueUser.save(flush: true)
        if(roleId){
            def role = Role.get(roleId)
            if(!role)
                throw new Exception("Ce rôle n'existe pas")
            userRoleService.update(trueUser, role)
        }
        return trueUser
    }



    User configurer(User user,boolean isPassword){
        User trueUser = User.findById(user.id)
        if(isPassword){
            trueUser.password = user.password
        }
        else{
            if(User.findByUsername(user.username)!=null){
                throw new Exception("Vous ne pouvez pas utiliser ce nom")
            }
            trueUser.username = user.username
        }
        if(!trueUser.validate()){
            throw new ValidationException('Une erreur de validation est survenue',trueUser.errors)
        }
        trueUser.save()
        return trueUser
    }
    def list(GrailsParameterMap params, String searchkey, String status){
        params.max = 10
        params.offset = params.offset ? params.offset.toInteger() : 0
        PagedResultList<User> userList = User.createCriteria().list(params) {
            if(searchkey != null && searchkey != "") {
                ilike("username", "%"+ searchkey +"%")
            }
            if(status != null && status != ""){
                eq("enabled", status.toBoolean())
            }
        }
        return [list: userList, count: userList.totalCount]
    }
}
