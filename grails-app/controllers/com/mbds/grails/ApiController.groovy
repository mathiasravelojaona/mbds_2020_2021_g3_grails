package com.mbds.grails

import Forms.Classes.InsertAnnonceForm
import Forms.Classes.CreateUserForm
import Forms.Classes.UpdateAnnonceForm
import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.plugin.springsecurity.rest.SpringSecurityRestGrailsPlugin
import grails.plugin.springsecurity.userdetails.GrailsUser
import grails.validation.ValidationException
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartRequest

import javax.servlet.http.HttpServletResponse

@Secured(['ROLE_ADMIN', 'ROLE_MODO'])
class ApiController {

    AnnonceService annonceService
    CustomAnnonceService customAnnonceService
    UserService userService
    UserRoleService userRoleService
    SpringSecurityService springSecurityService
    IllustrationService illustrationService
    CustomIllustrationService customIllustrationService
    CustomUserService customUserService

//    GET / PUT / PATCH / DELETE
//    url : localhost:8081/projet/api/annonce(s)/{id}
    def annonce() {
        try {
            if (!params.id)
                return response.status = HttpServletResponse.SC_BAD_REQUEST
            def annonce = annonceService.get(params.id)
            if (!annonce)
                return response.status = HttpServletResponse.SC_NOT_FOUND
            switch (request.getMethod()) {
                case "GET":
                    serializeData(annonce, request.getHeader("Accept"))
                    break
                case "PUT":
                    def jsonObject = request.JSON
                    def annonceForm = new UpdateAnnonceForm()
                    annonceForm.title = jsonObject.title
                    annonceForm.description = jsonObject.description
                    annonceForm.price = jsonObject.price
                    annonceForm.id = params.id
                    annonceForm.idAuthor = jsonObject.author
                    if(!annonceForm.validate()){
                        throw new ValidationException('Une erreur de validation est survenue',annonceForm.errors)
                    }
                    customAnnonceService.updateAnnonce(annonceForm)
                    response.status = HttpServletResponse.SC_OK
                    serializeData([message : "L'annonce "+params.id+" a été modifiée"], request.getHeader("Accept"))
                    break
                case "PATCH":
                    def jsonObject = request.JSON
                    if (jsonObject.title)
                        annonce.title = jsonObject.title
                    if (jsonObject.description)
                        annonce.description = jsonObject.description
                    if (jsonObject.price)
                        annonce.price = Double.valueOf(jsonObject.price)
                    if (jsonObject.author)
                        annonce.author = userService.get(Long.parseLong(jsonObject.author))
                    if (!annonce.validate()) {
                        throw new ValidationException('Une erreur de validation est survenue',annonce.errors)
                    }
                    annonceService.save(annonce)
                    response.status = HttpServletResponse.SC_OK
                    serializeData([message : "L'annonce "+params.id+" a été modifiée"], request.getHeader("Accept"))
                    break
                case "DELETE":
                    if (isUserModo())
                        return response.status = HttpServletResponse.SC_UNAUTHORIZED
                    annonceService.delete(params.id);
                    response.status = HttpServletResponse.SC_OK
                    serializeData([message : "L'annonce "+params.id+" a été supprimée"], request.getHeader("Accept"))
                    break
                default:
                    return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                    break
            }
            return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
        }
        catch (ValidationException validationException){
            response.status = HttpServletResponse.SC_BAD_REQUEST
            serializeData(validationException.errors, request.getHeader("Accept"))
        }
        catch(Exception ex){
            response.status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR
            return  serializeData([message : ex.getMessage()], request.getHeader("Accept"))
        }
    }

//    GET / POST
    def annonces() {
        try {
            switch (request.getMethod()) {
                case "GET":
                    def jsonObject = request.JSON
                    def max = 10
                    def page = jsonObject.page ? jsonObject.page as int : 0
                    def offset = max * page
                    def annoncesList = annonceService.list([max:max, offset: offset])
                    serializeData(annoncesList, request.getHeader("Accept"))
                    break
                case "POST":
                    if (isUserModo())
                        return response.status = HttpServletResponse.SC_UNAUTHORIZED
                    InsertAnnonceForm annonce = new InsertAnnonceForm()
                    annonce.title = params.title
                    annonce.description = params.description
                    annonce.price = params.price
                    annonce.idAuthor = params.author
                    MultipartFile picture = params.illustration
                    if (!annonce.validate()) {
                        throw new ValidationException('Une erreur de validation est survenue',annonce.errors)
                    }
                    def annonceInstance = customAnnonceService.SaveWithImage(annonce, picture)
                    response.status = HttpServletResponse.SC_CREATED;
                    serializeData(annonceInstance, request.getHeader("Accept"))
                    break
                default:
                    return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                    break
            }
            return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
        }
        catch (ValidationException validationException){
            response.status = HttpServletResponse.SC_BAD_REQUEST
            serializeData(validationException.errors, request.getHeader("Accept"))
        }
        catch(Exception ex){
            response.status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR
            return  serializeData([message : ex.getMessage()], request.getHeader("Accept"))
        }
    }

//    GET / PUT / PATCH / DELETE
    def user() {
        try {
            if (!params.id)
                return response.status = HttpServletResponse.SC_BAD_REQUEST
            def user = userService.get(params.id)
            if (!user)
                return response.status = HttpServletResponse.SC_NOT_FOUND
            switch (request.getMethod()) {
                case "GET":
                    serializeData(user, request.getHeader("Accept"))
                    break
                case "PUT":
                    def currentUser = springSecurityService.getPrincipal() as GrailsUser
                    user.discard()
                    def jsonObject = request.JSON
                    user.username = jsonObject.username
                    user.password = jsonObject.password
                    user.enabled = jsonObject.enabled
                    user.accountExpired = jsonObject.accountExpired
                    user.accountLocked = jsonObject.accountLocked
                    user.passwordExpired = jsonObject.passwordExpired
                    def roleId = Long.valueOf(jsonObject.role)
                    customUserService.updateUserAPI(user,roleId,user.id == currentUser.id)
                    response.status = HttpServletResponse.SC_OK
                    serializeData([message : "L'utilisateur "+params.id+" a été modifié"], request.getHeader("Accept"))
                    break
                case "PATCH":
                    def currentUser = springSecurityService.getPrincipal() as GrailsUser
                    user.discard()
                    def jsonObject = request.JSON
                    if (jsonObject.username)
                        user.username = jsonObject.username
                    if (jsonObject.password)
                        user.password = jsonObject.password
                    if (jsonObject.enabled)
                        user.enabled = jsonObject.enabled
                    if (jsonObject.accountExpired)
                        user.accountExpired = jsonObject.accountExpired
                    if (jsonObject.accountLocked)
                        user.accountLocked = jsonObject.accountLocked
                    if (jsonObject.passwordExpired)
                        user.password = jsonObject.passwordExpired
                    def roleId = null
                    if (jsonObject.role)
                        roleId = jsonObject.role
                    customUserService.updateUserAPI(user,roleId,user.id == currentUser.id)
                    response.status = HttpServletResponse.SC_OK
                    serializeData([message : "L'utilisateur "+params.id+" a été modifié"], request.getHeader("Accept"))
                    break;
                case "DELETE":
                    if (isUserModo())
                        return response.status = HttpServletResponse.SC_UNAUTHORIZED
                    def currentUser = springSecurityService.getPrincipal() as GrailsUser
                    if(user.id == currentUser.id){
                        throw new Exception('Vous ne pouvez pas vous supprimer')
                    }
                    userRoleService.deleteUser(user)
                    response.status = HttpServletResponse.SC_OK
                    serializeData([message : "L'utilisateur "+params.id+" a été supprimé"], request.getHeader("Accept"))
                    break
                default:
                    return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                    break
            }
            return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
        }
        catch (ValidationException validationException){
            response.status = HttpServletResponse.SC_BAD_REQUEST
            serializeData(validationException.errors, request.getHeader("Accept"))
        }
        catch(Exception ex){
            response.status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR
            return  serializeData([message : ex.getMessage()], request.getHeader("Accept"))
        }
    }

//    GET / POST
    def users() {
        try {
            switch (request.getMethod()) {
                case "GET":
                    def jsonObject = request.JSON
                    def max = 10
                    def page = jsonObject.page ? jsonObject.page as int : 0
                    def offset = max * page
                    def usersList = userService.list([max:max, offset:offset])
                    serializeData(usersList, request.getHeader("Accept"))
                    break
                case "POST":
                    if (isUserModo())
                        return response.status = HttpServletResponse.SC_UNAUTHORIZED
                    def jsonObject = request.JSON
                    def createUserForm = new CreateUserForm(username: jsonObject.username,
                            password:jsonObject.password,
                            role: jsonObject.role)
                    def user = userRoleService.createUser(createUserForm)
                    response.status = HttpServletResponse.SC_CREATED
                    serializeData(user, request.getHeader("Accept"))
                    break
                default:
                    return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                    break
            }
            return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
        }
        catch (ValidationException validationException){
            response.status = HttpServletResponse.SC_BAD_REQUEST
            serializeData(validationException.errors, request.getHeader("Accept"))
        }
        catch(Exception ex){
            response.status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR
            return  serializeData([message : ex.getMessage()], request.getHeader("Accept"))
        }
    }

    def illustration(){
        try {
            if (!params.id)
                return response.status = HttpServletResponse.SC_BAD_REQUEST
            switch (request.getMethod()) {
                case "GET":
                    def annonce = annonceService.get(params.id)
                    if (!annonce)
                        return response.status = HttpServletResponse.SC_NOT_FOUND
                    def jsonObject = request.JSON
                    def max = 10
                    def page = jsonObject.page ? jsonObject.page as int : 0
                    def offset = max * page
                    def illustrations = customIllustrationService.listAnnonceIllustrations(annonce,max,offset)
                    serializeData(illustrations, request.getHeader("Accept"))
                    break
                case "DELETE":
                    if (isUserModo())
                        return response.status = HttpServletResponse.SC_UNAUTHORIZED
                    illustrationService.delete(params.id)
                    response.status = HttpServletResponse.SC_OK
                    serializeData([message : "L'illustration "+params.id+" a été supprimé"], request.getHeader("Accept"))
                    break
                default:
                    return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                    break
            }
            return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
        }
        catch(Exception ex){
            response.status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR
            return  serializeData([message : ex.getMessage()], request.getHeader("Accept"))
        }
    }

    def illustrations(){
        try {
            switch (request.getMethod()) {
                case "GET":
                    def jsonObject = request.JSON
                    def max = 10
                    def page = jsonObject.page ? jsonObject.page as int : 0
                    def offset = max * page
                    def illustrations = illustrationService.list([max:max,offset: offset])
                    serializeData(illustrations, request.getHeader("Accept"))
                    break
                case "POST":
                    if (isUserModo())
                        return response.status = HttpServletResponse.SC_UNAUTHORIZED
                    def annonceId = params.annonce
                    MultipartRequest multipartRequest = request as MultipartRequest
                    MultipartFile multipartFile = multipartRequest.getFile("importFile")
                    def illustration = customIllustrationService.addIllustration(Long.valueOf(annonceId), multipartFile)
                    response.status = HttpServletResponse.SC_CREATED;
                    serializeData(illustration, request.getHeader("Accept"))
                    break
                default:
                    return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                    break
            }
            return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
        }
        catch(Exception ex){
            response.status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR
            return  serializeData([message : ex.getMessage()], request.getHeader("Accept"))
        }
    }
    def serializeData(object, format)
    {
        switch (format)
        {
            case 'json':
            case 'application/json':
            case 'text/json':
                render object as JSON
                break
            case 'xml':
            case 'application/xml':
            case 'text/xml':
                render object as XML
                break
            default:
                render object as JSON
                break
        }
    }

    protected boolean isUserModo(){
        def currentUser = springSecurityService.getPrincipal() as GrailsUser
        def userRoles = currentUser.getAuthorities()
        return userRoles.size() == 1 && userRoles[0].authority == Role.ROLE_MODO
    }
}
