<%--
  Created by IntelliJ IDEA.
  User: john
  Date: 12/03/2021
  Time: 13:28
--%>

<!DOCTYPE html>

<html lang="en" class="material-style layout-fixed">

<head>
    <title>Lecoincoin - Administration</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="Empire Bootstrap admin template made using Bootstrap 4, it has tons of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
    <meta name="keywords" content="Empire, bootstrap admin template, bootstrap admin panel, bootstrap 4 admin template, admin template">
    <meta name="author" content="Srthemesvilla" />
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico">

    <!-- Google fonts -->

    <!-- Icon fonts -->
    <asset:stylesheet rel="stylesheet" href="template-1/fonts/fontawesome.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/fonts/ionicons.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/fonts/linearicons.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/fonts/open-iconic.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/fonts/pe-icon-7-stroke.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/fonts/feather.css" />

    <!-- Core stylesheets -->
    <asset:stylesheet rel="stylesheet" href="template-1/bootstrap-material.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/shreerang-material.css" />
    <asset:stylesheet rel="stylesheet" href="template-1/uikit.css" />

    <!-- Libs -->
    <asset:stylesheet rel="stylesheet" href="template-1/libs/perfect-scrollbar/perfect-scrollbar.css" />
    <!-- Page -->
    <asset:stylesheet rel="stylesheet" href="template-1/pages/authentication.css" />
</head>

<body>
<!-- [ Preloader ] Start -->
<div class="page-loader">
    <div class="bg-primary"></div>
</div>
<!-- [ Preloader ] End -->

<!-- [ content ] Start -->
<div class="authentication-wrapper authentication-1 px-4">
    <div class="authentication-inner py-5">

        <!-- [ Logo ] Start -->
        <div class="d-flex justify-content-center align-items-center">
            <div class="ui-w-60">
                <div class="w-100 position-relative">
                    <img src="${grailsApplication.config.annonces.illustrations.url + 'template-1/logo-dark.png'}" alt="Brand Logo" class="img-fluid">
                </div>
            </div>
        </div>
        <!-- [ Logo ] End -->

        <!-- [ Form ] Start -->
    <g:form class="my-5" controller="login" action="authenticate">
            <div class="form-group">
                <label class="form-label">Nom</label>
                <input type="text" name="username" id="username" class="form-control">
                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <label class="form-label d-flex justify-content-between align-items-end">
                    <span>Mot de passe</span>
                </label>
                <input type="password" name="password" id="password" class="form-control">
                <div class="clearfix"></div>
            </div>
            <div class="d-flex justify-content-between align-items-center m-0">
                <g:actionSubmit class="btn btn-primary" value="Connexion" style="width:100%" />
            </div>
    </g:form>

        <!-- [ Form ] End -->


        <g:if test='${flash.message}'>
            <p style="color: red" class="text-center error-msg-register">Cet utilisateur n'existe pas</p>
        </g:if>
    </div>
</div>
<!-- [ content ] End -->
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<!-- Core scripts -->
<asset:javascript src="template-1/pace.js" />
<asset:javascript src="template-1/libs/popper/popper.js"/>
<asset:javascript src="template-1/bootstrap.js"/>
<asset:javascript src="template-1/sidenav.js"/>
<asset:javascript src="template-1/layout-helpers.js"/>
<asset:javascript src="template-1/material-ripple.js"/>


<!-- Libs -->
<asset:javascript src="template-1/libs/perfect-scrollbar/perfect-scrollbar.js"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

<!-- Demo -->
</body>

</html>
